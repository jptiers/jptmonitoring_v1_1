#include "jptthreadcounters.h"
#include "QDateTime"
#include "QDebug"

const int delay(10000);
//Constructor
//Thread that allows reading the files at different times
jptthreadcounters::jptthreadcounters(){}

void jptthreadcounters::run(){
    for(;;)
        process_run();
}

void jptthreadcounters::process_run(){
    //qDebug()<<"HILO EXE";
    QTime time = QTime::currentTime();
    QString time_string = time.toString();

    process_run_24h(time_string);
    process_run_12h(time_string);
    process_run_1h(time_string);
    msleep(delay);
}

void jptthreadcounters::process_run_24h(QString time){
    //List for 24 hours
    a_logs_counters = new logs_counters();

    QStringList dates_24h;
    dates_24h.append("23:59:50");
    dates_24h.append("23:59:51");
    dates_24h.append("23:59:52");
    dates_24h.append("23:59:53");
    dates_24h.append("23:59:54");
    dates_24h.append("23:59:55");
    dates_24h.append("23:59:56");
    dates_24h.append("23:59:57");
    dates_24h.append("23:59:58");
    dates_24h.append("23:59:59");

    for(int i=0;i<10;i++){
        //for 24 hours
        if(dates_24h[i] == time){
            qDebug()<<"timeeeee 24 h"<<time;
            a_logs_counters->clean_all_counters();

            if(a_logs_counters->count_file_good_bd()){
                qDebug()<<"COUNTER GOOD BD "<<a_logs_counters->get_counter_good_db();
            }
            if(a_logs_counters->count_file_bad_bd()){
                qDebug()<<"COUNTER BAD BD "<<a_logs_counters->get_counter_bad_db();
            }
            if(a_logs_counters->count_file_good_cloud()){
                qDebug()<<"COUNTER GOOD CLOUD "<<a_logs_counters->get_counter_good_cloud();
            }
            if(a_logs_counters->count_file_bad_cloud()){
                qDebug()<<"COUNTER BAD CLOUD "<<a_logs_counters->get_counter_bad_cloud();
            }
            emit send_cloud_json_counters(a_logs_counters->get_counter_good_db(), a_logs_counters->get_counter_bad_db(),
                                          a_logs_counters->get_counter_good_cloud(),a_logs_counters->get_counter_bad_cloud());
            a_logs_counters->clean_all_files();
        }
    }
}

void jptthreadcounters::process_run_12h(QString time){
    //List for 12 hours
    a_logs_counters = new logs_counters();

    QStringList dates_12h;
    dates_12h.append("11:59:50");
    dates_12h.append("11:59:51");
    dates_12h.append("11:59:52");
    dates_12h.append("11:59:53");
    dates_12h.append("11:59:54");
    dates_12h.append("11:59:55");
    dates_12h.append("11:59:56");
    dates_12h.append("11:59:57");
    dates_12h.append("11:59:58");
    dates_12h.append("11:59:59");

    for(int i=0;i<10;i++){
        //for 24 hours
        if(dates_12h[i] == time){
            qDebug()<<"timeeeee 12 h"<<time;
            a_logs_counters->clean_all_counters();

            if(a_logs_counters->count_file_good_bd()){
                qDebug()<<"COUNTER GOOD BD "<<a_logs_counters->get_counter_good_db();
            }
            if(a_logs_counters->count_file_bad_bd()){
                qDebug()<<"COUNTER BAD BD "<<a_logs_counters->get_counter_bad_db();
            }
            if(a_logs_counters->count_file_good_cloud()){
                qDebug()<<"COUNTER GOOD CLOUD "<<a_logs_counters->get_counter_good_cloud();
            }
            if(a_logs_counters->count_file_bad_cloud()){
                qDebug()<<"COUNTER BAD CLOUD "<<a_logs_counters->get_counter_bad_cloud();
            }
            emit send_cloud_json_counters(a_logs_counters->get_counter_good_db(), a_logs_counters->get_counter_bad_db(),
                                          a_logs_counters->get_counter_good_cloud(),a_logs_counters->get_counter_bad_cloud());
        }
    }
}

void jptthreadcounters::process_run_1h(QString time){
    //List for 1 hour
    a_logs_counters = new logs_counters();

    QStringList split_time = time.split(":");
    QString reset_time = split_time[0] + ":" + split_time[1];
    QString reset_hour = split_time[0];

    QStringList dates_1h;
    dates_1h.append(reset_hour+":59:50");
    dates_1h.append(reset_hour+":59:51");
    dates_1h.append(reset_hour+":59:52");
    dates_1h.append(reset_hour+":59:53");
    dates_1h.append(reset_hour+":59:54");
    dates_1h.append(reset_hour+":59:55");
    dates_1h.append(reset_hour+":59:56");
    dates_1h.append(reset_hour+":59:57");
    dates_1h.append(reset_hour+":59:58");
    dates_1h.append(reset_hour+":59:59");

    if(reset_time != "11" && reset_time != "23"){
        for(int i=0;i<10;i++){
            //for 1 hour
            if(dates_1h[i] == time){
                qDebug()<<"timeeeee 1 h"<<time;
                a_logs_counters->clean_all_counters();

                if(a_logs_counters->count_file_good_bd()){
                    qDebug()<<"COUNTER GOOD BD "<<a_logs_counters->get_counter_good_db();
                }
                if(a_logs_counters->count_file_bad_bd()){
                    qDebug()<<"COUNTER BAD BD "<<a_logs_counters->get_counter_bad_db();
                }
                if(a_logs_counters->count_file_good_cloud()){
                    qDebug()<<"COUNTER GOOD CLOUD "<<a_logs_counters->get_counter_good_cloud();
                }
                if(a_logs_counters->count_file_bad_cloud()){
                    qDebug()<<"COUNTER BAD CLOUD "<<a_logs_counters->get_counter_bad_cloud();
                }
                int up_hour = reset_hour.toInt();
                if(up_hour == 23){
                    reset_hour = "00";
                }else{
                    reset_hour = QString::number(up_hour++);
                }
                emit send_cloud_json_counters(a_logs_counters->get_counter_good_db(), a_logs_counters->get_counter_bad_db(),
                                              a_logs_counters->get_counter_good_cloud(),a_logs_counters->get_counter_bad_cloud());
            }
        }
    }else{
        int up_hour = reset_hour.toInt();
        if(up_hour == 11){
            reset_hour = QString::number(up_hour++);
        }else{
            reset_hour = "00";
        }
    }
}



