#ifndef JPTTHREADCOUNTERS_H
#define JPTTHREADCOUNTERS_H

#include <QThread>
#include <QObject>
#include "class/logs/logs_counters.h"

//This class is the one that supervises the system hour
class jptthreadcounters: public QThread{
    Q_OBJECT
public:
    jptthreadcounters();
    //This method contains the validation hour
    void run();
    void process_run();
    void process_run_24h(QString time);
    void process_run_12h(QString time);
    void process_run_1h(QString time);

signals:
    void send_cloud_json_counters(int good_db, int bad_bd, int good_cloud, int bad_cloud);

private:
    logs_counters *a_logs_counters;
};

#endif // JPTTHREADCOUNTERS_H
