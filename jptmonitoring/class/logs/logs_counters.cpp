#include "logs_counters.h"
#include <QFile>
#include <QDebug>

const QString name_file_good_bd = "logs_good_bd";
const QString name_file_bad_bd = "logs_bad_bd";
const QString name_file_good_cloud = "logs_good_cloud";
const QString name_file_bad_cloud = "logs_bad_cloud";
const QString path_source = "/jpt/jptmonitoring/External/Logs/";
const QString extension = ".txt";

logs_counters::logs_counters(){}

//Definitions the methods for counters
void logs_counters::set_counter_good_db(int value){
    counter_good_bd = QString::number(value);
}
int logs_counters::get_counter_good_db(){
    return counter_good_bd.toInt();
}
void logs_counters::set_counter_bad_db(int value){
    counter_bad_bd = QString::number(value);
}
int logs_counters::get_counter_bad_db(){
    return counter_bad_bd.toInt();
}
void logs_counters::set_counter_good_cloud(int value){
    counter_good_cloud = QString::number(value);
}
int logs_counters::get_counter_good_cloud(){
    return counter_good_cloud.toInt();
}
void logs_counters::set_counter_bad_cloud(int value){
    counter_bad_cloud = QString::number(value);
}
int logs_counters::get_counter_bad_cloud(){
    return counter_bad_cloud.toInt();
}

void logs_counters::clean_all_counters(){
    set_counter_good_db(0);
    set_counter_bad_db(0);
    set_counter_good_cloud(0);
    set_counter_bad_cloud(0);
}

void logs_counters::save_count_good_bd(QString time, QString trame_json){
    //Create route where this located the file
    QString path = path_source + name_file_good_bd + extension;
    //Evaluate if exist file
    QFile file(path);
    //Apply in the conditional
    if(!file.exists()){
        //If not exist file, create one
        qDebug()<<"SE CREA ARCHIVO GOOD BD";
        QFile create_file(path_source+name_file_good_bd+extension);
        if(create_file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)){
            //How is the first time
            qDebug()<<"PRIMERA VEZ GOOD BD";
            QTextStream input_data(&create_file);
            //Write data into file after created
            input_data<<time+"||"+trame_json<<endl;
            //Close file
            create_file.close();
        }
    }else{
        if(file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)){
            qDebug()<<"ALMACENANDO EN GOOD BD";
            QTextStream input_data(&file);
            input_data<<time+"||"+trame_json<<endl;
            file.close();
        }
    }
}

void logs_counters::save_count_bad_bd(QString time, QString trame_json){
    //Make same in this for the bad counter in local bd
    QString path = path_source + name_file_bad_bd + extension;
    QFile file(path);
    
    if(!file.exists()){
        qDebug()<<"SE CREAR ARCHIVO BAD BD";
        QFile create_file(path_source+name_file_bad_bd+extension);
        if(create_file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)){
            qDebug()<<"PRIMERA VEZ BAD DB";
            QTextStream input_data(&create_file);
            input_data<<time+"||"+trame_json<<endl;
            create_file.close();
        }
    }else{
        if(file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)){
            qDebug()<<"ALMANCENADO EN BAD DB";
            QTextStream input_data(&file);
            input_data<<time+"||"+trame_json<<endl;
            file.close();
        }
    }
}

void logs_counters::save_count_good_cloud(QString time, QString trame_json){
    //Create route where this located the file
    QString path = path_source + name_file_good_cloud + extension;
    //Evaluate if exist file
    QFile file(path);
    //Apply in the conditional
    if(!file.exists()){
        //If not exist file, create one
        qDebug()<<"SE CREA ARCHIVO GOOD CLOUD";
        QFile create_file(path_source+name_file_good_cloud+extension);
        if(create_file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)){
            //How is the first time
            qDebug()<<"PRIMERA VEZ GOOD CLOUD";
            QTextStream input_data(&create_file);
            //Write data into file after created
            input_data<<time+"||"+trame_json<<endl;
            //Close file
            create_file.close();
        }
    }else{
        if(file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)){
            qDebug()<<"ALMACENANDO EN GOOD CLOUD";
            QTextStream input_data(&file);
            input_data<<time+"||"+trame_json<<endl;
            file.close();
        }
    }
}

void logs_counters::save_count_bad_cloud(QString time, QString trame_json){
    //Make same in this for the bad counter in the cloud
    QString path = path_source + name_file_bad_cloud + extension;
    QFile file(path);
    
    if(!file.exists()){
        qDebug()<<"SE CREAR ARCHIVO BAD CLOUD";
        QFile create_file(path_source+name_file_bad_cloud+extension);
        if(create_file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)){
            qDebug()<<"PRIMERA VEZ BAD CLOUD";
            QTextStream input_data(&create_file);
            input_data<<time+"||"+trame_json<<endl;
            create_file.close();
        }
    }else{
        if(file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)){
            qDebug()<<"ALMANCENADO EN BAD CLOUD";
            QTextStream input_data(&file);
            input_data<<time+"||"+trame_json<<endl;
            file.close();
        }
    }
}

bool logs_counters::count_file_good_bd(){
    try {
        QString line("");
        QString path = path_source + name_file_good_bd + extension;
        QFile file(path);
        //In case file exists, come in this part and read into file
        if(file.open(QIODevice::ReadOnly)){
            QTextStream get(&file);
            int counter = 0;
            while(!get.atEnd()){
                //In this part start count
                line = get.readLine();
                counter+=1;
            }
            qDebug()<<"Contador good bd "<<counter;
            set_counter_good_db(counter);
        }
        return true;
    } catch (...) {
        return false;
    }
}

bool logs_counters::count_file_bad_bd(){
    try {
        QString line("");
        QString path = path_source + name_file_bad_bd + extension;
        QFile file(path);
        //In case file exists, come in this part and read into file
        if(file.open(QIODevice::ReadOnly)){
            QTextStream get(&file);
            int counter = 0;
            while(!get.atEnd()){
                //In this part start count
                line = get.readLine();
                counter+=1;
            }
            qDebug()<<"Contador de bad bd "<<counter;
            set_counter_bad_db(counter);
        }
        return true;
    } catch (...) {
        return false;
    }
}

bool logs_counters::count_file_good_cloud(){
    try {
        QString line("");
        QString path = path_source + name_file_good_cloud + extension;
        QFile file(path);
        //In case file exists, come in this part and read into file
        if(file.open(QIODevice::ReadOnly)){
            QTextStream get(&file);
            int counter = 0;
            while(!get.atEnd()){
                //In this part start count
                line = get.readLine();
                counter+=1;
            }
            qDebug()<<"Contador de good cloud "<<counter;
            set_counter_good_cloud(counter);
        }
        return true;
    } catch (...) {
        return false;
    }

}

bool logs_counters::count_file_bad_cloud(){
    try {
        QString line("");
        QString path = path_source + name_file_bad_cloud + extension;
        QFile file(path);
        //In case file exists, come in this part and read into file
        if(file.open(QIODevice::ReadOnly)){
            QTextStream get(&file);
            int counter = 0;
            while(!get.atEnd()){
                //In this part start count
                line = get.readLine();
                counter+=1;
            }
            qDebug()<<"Contador de bad cloud "<<counter;
            set_counter_bad_cloud(counter);
        }
        return true;
    } catch (...) {
        return false;
    }

}

void logs_counters::clean_all_files(){
    system("rm -r /jpt/jptmonitoring/External/Logs/logs_good_bd.txt");
    system("rm -r /jpt/jptmonitoring/External/Logs/logs_good_cloud.txt");
    system("rm -r /jpt/jptmonitoring/External/Logs/logs_bad_bd.txt");
    system("rm -r /jpt/jptmonitoring/External/Logs/logs_bad_cloud.txt");
}
