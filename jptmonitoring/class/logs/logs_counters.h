#ifndef LOGS_COUNTERS_H
#define LOGS_COUNTERS_H
#include <QObject>

class logs_counters
{
public:
    logs_counters();

    //Methods for save in file trame json with time, where save in local database.
    void save_count_good_bd(QString time, QString trame_json);
    void save_count_bad_bd(QString time, QString trame_json);
    //Methods for save in file trame json with time, where upload in the cloud (SWARM).
    void save_count_good_cloud(QString time, QString trame_json);
    void save_count_bad_cloud(QString time, QString trame_json);
    //Methods set and get for counters
    //Set and get for counter good database
    void set_counter_good_db(int value);
    int get_counter_good_db();
    //Set and get for counter bad database
    void set_counter_bad_db(int value);
    int get_counter_bad_db();
    //Set and get for counter good cloud
    void set_counter_good_cloud(int value);
    int get_counter_good_cloud();
    //Set and get for counter bad cloud
    void set_counter_bad_cloud(int value);
    int get_counter_bad_cloud();
    //Methods for read the files and count the lines inside the file
    bool count_file_good_bd();
    bool count_file_bad_bd();
    bool count_file_good_cloud();
    bool count_file_bad_cloud();
    //Method for clear all counters
    void clean_all_counters();
    //Method to clean the logs files
    void clean_all_files();

private:
    QString counter_good_bd;
    QString counter_bad_bd;
    QString counter_good_cloud;
    QString counter_bad_cloud;
};

#endif // LOGS_COUNTERS_H
