#include "jptdatabase.h"
#include <QDebug>


const QString           gv_list_name_tables[6] = {"Functions","Configurations","Characteristics","Modules","PWM","Delayer"};
//*********************************************************************************************************************************************************
// Constructor
//*********************************************************************************************************************************************************
jptdatabase::jptdatabase(QString name_data_base, QString password, QString user_name, QObject *parent) : QObject(parent){
    db = QSqlDatabase::addDatabase("QSQLITE");

#ifdef _HUB_ON

    //In this line uncomment in case
    //db.setDatabaseName("/media/debian/JPTSD/" + name_data_base);
    db.setDatabaseName("/jpt/jptmonitoring/External/" + name_data_base);
    //db.setDatabaseName("/home/debian/Desktop/JPTSD/" + name_data_base);

#else
    db.setDatabaseName("/jpt/jptmonitoring/External/" + name_data_base);

#endif
    db.setPassword(password);
    db.setUserName(user_name);
}
//*********************************************************************************************************************************************************
// Inicializa la conexion con la base de datos
//*********************************************************************************************************************************************************
void jptdatabase::init_database_connection(){
#ifndef _COMPILE_MESSAGE
    db.open();
#else
    if(db.open())
        emit viewer_status("[o] Database");
    else
        emit viewer_status("[*] Database");
#endif
}
//*********************************************************************************************************************************************************
// Destructor
//*********************************************************************************************************************************************************
jptdatabase::~jptdatabase(){
    db.close();
}


//*********************************************************************************************************************************************************
// Get and set for json trame and time
//*********************************************************************************************************************************************************
void jptdatabase::set_date_time(QString time){
    datetime = time.trimmed();
}
QString jptdatabase::get_date_time(){
    return datetime;
}
void jptdatabase::set_json_trame(QString json){
    json_trame = json;
}
QString jptdatabase::get_json_trame(){
    return json_trame;
}
//*********************************************************************************************************************************************************
// Insertar Datos en las tablas
//*********************************************************************************************************************************************************
void jptdatabase::insert_register(QString name_table, QString query){
    QString new_query;
    new_query.append("INSERT INTO " + name_table + query);
    QSqlQuery create;
    create.prepare(new_query);
    //Initialize class logs counters and save information
    logs_counters a_logs_counters;

    try {
        if(create.exec()){
            a_logs_counters.save_count_good_bd(get_date_time(), get_json_trame());
        }else{
            a_logs_counters.save_count_bad_bd(get_date_time(), get_json_trame());
        }
    } catch (...) {
        a_logs_counters.save_count_bad_bd(get_date_time(),get_json_trame());
    }
}

//*********************************************************************************************************************************************************
// Obtener los registros necesitados
//*********************************************************************************************************************************************************
void jptdatabase::get_values(int pos_name_table, QVector<QStringList > *vec_data){
    vec_data->clear();

    QString lv_name_table(gv_list_name_tables[pos_name_table]);
    int lv_num_item_table(0);

    if(pos_name_table == def_pos_table_func)
        lv_num_item_table  = def_num_colums_table_func;
    else if(pos_name_table == def_pos_table_conf)
        lv_num_item_table  = def_num_colums_table_conf;
    else if(pos_name_table == def_pos_table_char)
        lv_num_item_table  = def_num_colums_table_char;
    else if(pos_name_table == def_pos_table_modu)
        lv_num_item_table  = def_num_colums_table_modu;
    else if(pos_name_table == def_pos_table_pwms)
        lv_num_item_table  = def_num_colums_table_pwms;
    else if(pos_name_table == def_pos_table_dela)
        lv_num_item_table  = def_num_colums_table_dela;

    QString lv_query;
    lv_query.append("SELECT * FROM " + lv_name_table + " ORDER BY id ");
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    if(lv_get_query.exec())
        while(lv_get_query.next()){
            QStringList lv_row_data;
            for(int f_colum = 1; f_colum < lv_num_item_table; ++f_colum)
                lv_row_data << lv_get_query.value(f_colum).toString();
            vec_data->append(lv_row_data);
        }

}
//*********************************************************************************************************************************************************
// Actualizar los registros de la base de datos
//*********************************************************************************************************************************************************
void jptdatabase::update_register(QString name_table, QString query){
    QString lv_query;
    lv_query.append("UPDATE " + name_table + " SET " + query + ";");
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    lv_get_query.exec();

}
//*********************************************************************************************************************************************************
// Actualizar los registros de la base de datos
//*********************************************************************************************************************************************************
void jptdatabase::update_register(QString name_table, QString id, QString query){
    QString lv_query;
    lv_query.append("UPDATE " + name_table + " SET " + query + " WHERE id=" + id + ";");
    QString dattt="UPDATE " + name_table + " SET " + query + " WHERE id=" + id + ";";
    qDebug()<<dattt;

    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    if(lv_get_query.exec())
    {
        qDebug()<<"Success Query";
    }
}

void jptdatabase::update_hub_name(QString hub_name)
{
    QString lv_query;

    lv_query="UPDATE Configurations SET hub_name='"+hub_name+"'  WHERE id=1";
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    if(lv_get_query.exec())
    {
        qDebug()<<"Success Query: hub_name";
    }

}


void jptdatabase::update_ip_cloud(QString ip_cloud)
{
    QString lv_query;

    lv_query="UPDATE Configurations SET ip_server='"+ip_cloud+"'  WHERE id=1";
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    if(lv_get_query.exec())
    {
        qDebug()<<"Success Query: ip_cloud";
    }

}


void jptdatabase::update_port_cloud(QString port_cloud)
{
    QString lv_query;

    lv_query="UPDATE Configurations SET port_server='"+port_cloud+"'  WHERE id=1";
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    if(lv_get_query.exec())
    {
        qDebug()<<"Success Query: port_cloud";
    }

}

QStringList jptdatabase::get_modules_from_db()
{
    QStringList _result;
    QString lv_query;

    lv_query="select (name_module) from Modules;";
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    if(lv_get_query.exec())
    {
        qDebug()<<"Success Query: ip_cloud";
        while(lv_get_query.next())
        {
            QString _temp=lv_get_query.value(0).toString();
            _result.append(_temp);
            //qDebug()<<lv_get_query.value(0)<<"*********************7777777777777777777******************************";
        }
    }
    return _result;

}
//GET PARAMS FOR CONFIGURATION MODBUS
QStringList jptdatabase::get_modbus_conf(){
    QStringList _result;
    QString lv_query;

    lv_query="select * from modbus_config;";
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    if(lv_get_query.exec())
    {
        qDebug()<<"Success Query: Modbus Conf";
        while(lv_get_query.next())
        {
            _result.append(lv_get_query.value(1).toString());
            _result.append(lv_get_query.value(2).toString());
            _result.append(lv_get_query.value(3).toString());
            _result.append(lv_get_query.value(4).toString());
            _result.append(lv_get_query.value(5).toString());
            _result.append(lv_get_query.value(6).toString());
            _result.append(lv_get_query.value(7).toString());
        }
    }
    return _result;
}
//SET UPDATE PARAMS FOR CONFIGURATION MODBUS
void jptdatabase::set_modbus_conf(QString parity, QString baud, QString data_bits, QString stop_bits, QString port, QString ip, QString port_ip){
    QString lv_query;

    lv_query="UPDATE modbus_config SET parity='"+parity+"', baud_rate="+baud+", data_bits="+data_bits+", stop_bits="+stop_bits+", port='"+port+"', ip='"+ip+"', ip_port='"+port_ip+"' WHERE id=1";
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    if(lv_get_query.exec())
    {
        qDebug()<<"Success Query: Modbus Configuration Update";
    }
}
//GET VALUES WIDGETS
QStringList jptdatabase::get_data_widget_modbus(QString m_device){
    QStringList _result;
    QString lv_query;

    lv_query="SELECT * FROM Nodex WHERE m_device ='"+m_device+"' ORDER BY id DESC LIMIT 1;";
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    if(lv_get_query.exec())
    {
        qDebug()<<"Success Query: Widget Nodex";
        while(lv_get_query.next())
        {
            _result.append(lv_get_query.value(1).toString());
            _result.append(lv_get_query.value(2).toString());
            _result.append(lv_get_query.value(3).toString());
            _result.append(lv_get_query.value(4).toString());
        }
    }
    return _result;
}

void jptdatabase::set_table_modbus(QString date, QString data_table){

    QString lv_query;

    lv_query="INSERT INTO modbus_table (date, data_table) values ('"+date+"', '"+data_table+"')";
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    if(lv_get_query.exec())
    {
        qDebug()<<"Success Query: Modbus Table Modbus Update";
    }
}

QString jptdatabase::clean_database(){
    QString lv_query;
    QString lv_query_vacumm;

    lv_query="DELETE FROM Nodex;";
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    if(lv_get_query.exec()){
        lv_query_vacumm = "VACUUM;";

        QSqlQuery lv_get_query_va;
        lv_get_query_va.setForwardOnly(true);
        lv_get_query_va.prepare(lv_query);
        if(lv_get_query_va.exec()){
            qDebug()<<"Success Query: Clean database";
            return "Database cleaned sucefully!";
        }
    }
}


