#ifndef JPTPWMMA_H
#define JPTPWMMA_H

#include <QObject>
#include <_sentence_.h>

#define def_min_ran_pwm 0
#define def_max_ran_pwm 1

#define def_name_pin_pwm 0
#define def_posi_pin_pwm 1

class jptpwmma : public QObject{
    Q_OBJECT

private:
#ifdef _HUB_ON
    void init_pin_pwm(int *pos_pin);
#endif

public:
    explicit jptpwmma(QObject *parent = nullptr);
    void configure_pin_pwm(QString pin_pwm, QString range);

signals:
#ifdef _COMPILE_MESSAGE
    void viewer_status(QString status);
#endif
public slots:
    void set_value_pwm(QString value, QString pin_pwm);
};

#endif // JPTPWMMA_H
