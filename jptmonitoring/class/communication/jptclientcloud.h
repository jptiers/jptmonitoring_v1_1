#ifndef JPTCLIENTCLOUD_H
#define JPTCLIENTCLOUD_H

#include <QObject>
#include <QStack>
#include <QTimer>

#include <QtNetwork/QtNetwork>
#include <QtNetwork/QNetworkInterface>

#include "class/extern/jptethernet.h"
#include "class/extern/jptblinkdata.h"
#include "class/extern/jptenergylevel.h"
#include "class/logs/logs_counters.h"
#include "_sentence_.h"

class jptclientcloud : public QObject{
    Q_OBJECT

public:
    explicit jptclientcloud(const QString ip_cloud, const int port_cloud, const int port_reco, QObject *parent = nullptr);
    ~jptclientcloud();
    void set_ip_cloud(QString ip_cloud);
    void set_port_cloud(int port);
    void set_port_recovery_cloud(int port_reco);
    void system_restart();
    QString message_json;
    void stop_all();

signals:
#ifdef _COMPILE_MESSAGE
    void viewer_status(QString status);
#endif
    void status_ethernet(bool status, int id); // Lanza el estado del internet proveniente de jptethernet SIGNAL SLOT SIGNAL (bool)
    void status_cloud(bool status);    // Lanza el estado de la conexion al servidor, se hace en base a la comunicacion con internet

    void value_volts_and_current_signal(QString value_1, QString value_2, QString name_node);
    void status_voltage(bool status, int id);
    void restart_beagle_signal_ethernet();
    void check_model_old();


public slots:
#ifdef _COMPILE_MESSAGE
    void viewer_status_(QString status);
#endif
    void status_ethernet_(bool status); // Slot que recibe el estado del internet proveniente de jptethernet {SIGNAL status_ethernet(bool) -> SLOT status_ethernet_}

    static void restart_connection_ethernet(){
#ifdef _HUB_ON
        system("sudo systemctl stop networking.service");
        system("sudo cp interfaces /etc/network/interfaces");
        system("sudo systemctl start networking.service");
        system("sudo systemctl daemon-reload");
#endif
    }
    void stack_data(QString message);

    void message_from_server();
    void message_to_server();

    //recovery port
    void message_from_server_reco();
    void message_to_server_reco();

    void read_volt_hub();

    void value_volts_and_current_slot(QString value_1, QString value_2, QString name_node);
#ifdef _COMPILE_MESSAGE
    void display_error(QAbstractSocket::SocketError socketError);
    void display_error_reco(QAbstractSocket::SocketError socketError);
#endif

    void restart_beagle_signal();

    void _check_model_old();
private:
    QString a_ip_cloud;
    int a_port_cloud;
    int a_port_recovery;

    QStack<QString> a_buffer_data;
    QStack<QString> a_buffer_data_reco;

    QTcpSocket *a_socket_client;
    QTcpSocket *a_socket_client_reco;
    jptethernet *a_watch_status_ethernet;
    logs_counters *a_logs_counters;
    bool a_status_ethernet = false;
    bool a_status_cloud    = false;
    bool a_status_cloud_reco    = false;

    void restart_socket();
    void send_message_cloud(QString message);
    void send_message_cloud_reco(QString message);
    void check_connect_cloud();
};

#endif // JPTCLIENTCLOUD_H
