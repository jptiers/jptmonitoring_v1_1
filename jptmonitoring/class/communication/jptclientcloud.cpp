#include "jptclientcloud.h"
#include <QDebug>
#include <QDateTime>

#define def_delay_send_message    1000
#define def_delay_send_node       1600000
#define def_lim_voltaje_requerido 1200
#define def_wait_conn_Host        20000

bool gv_status_client = false;
QTimer *gv_timer_send_cloud;
QTimer *gv_timer_send_cloud_reco;
QTimer *gv_timer_level_energy;
bool flag_busy = false;
int counter = 0;

QString json_reco ="{\"data\":[";
QString json_total ="";
QString json_reco_final = "]}";
bool flag_time_out = true;
bool flag_time_out_reco = true;
int counter_send = 0;
int counter_send_reco = 0;

//******************************************************************************************************************************
// Funcion principal                                                                                            [jptclientcloud]
//******************************************************************************************************************************
jptclientcloud::jptclientcloud(const QString ip_cloud, const int port_cloud, const int port_reco, QObject *parent) : QObject(parent),
    a_ip_cloud(ip_cloud), a_port_cloud(port_cloud), a_port_recovery(port_reco){
    //----------------------------------------------------------------------------------------------------------------
    // conexion con el servidor nube                                                                      [QTcpSocket]
    //----------------------------------------------------------------------------------------------------------------
    a_socket_client = new QTcpSocket(this);
    a_socket_client_reco = new QTcpSocket(this);



    connect(a_socket_client, SIGNAL(readyRead()), this, SLOT(message_from_server()));
    connect(a_socket_client_reco, SIGNAL(readyRead()), this, SLOT(message_from_server_reco()));
#ifdef _COMPILE_MESSAGE
    connect(a_socket_client, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(display_error(QAbstractSocket::SocketError)));
    //listener para recovery
    connect(a_socket_client_reco, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(display_error_reco(QAbstractSocket::SocketError)));
#endif
    a_socket_client->connectToHost(QHostAddress(ip_cloud), port_cloud);
    //qDebug()<<a_socket_client->state()<<ip_cloud<<port_cloud;

    //conexion con el cliente de recovey
    a_socket_client_reco->connectToHost(QHostAddress(ip_cloud), port_reco);
    //qDebug()<<"RECO: "<<a_socket_client_reco->state()<<ip_cloud<<port_reco;
    //forzamos la conexion con el servidor
    //qDebug()<<"starting the socket TCP";
    //restart_socket();

    //----------------------------------------------------------------------------------------------------------------
    // Thread que vigila la conexion a internet                                                          [jptethernet]
    //----------------------------------------------------------------------------------------------------------------
    a_watch_status_ethernet = new jptethernet();
#ifdef _COMPILE_MESSAGE
    connect(a_watch_status_ethernet, SIGNAL(viewer_status(QString)), this, SLOT(viewer_status_(QString)));
#endif
    connect(a_watch_status_ethernet, SIGNAL(status_ethernet(bool)), this, SLOT(status_ethernet_(bool)));
    //connect(a_watch_status_ethernet, SIGNAL(restart_beagle_signal()), this, SLOT(restart_beagle_signal()));

    connect(a_watch_status_ethernet, SIGNAL(check_model_old()), this, SLOT(_check_model_old()));
    a_watch_status_ethernet->start();

    //----------------------------------------------------------------------------------------------------------------
    // Timer que envia los datos segun un tiempo                                                              [QTimer]
    //----------------------------------------------------------------------------------------------------------------
    gv_timer_send_cloud = new QTimer();
    //gv_timer_send_cloud->setInterval(def_delay_send_message);
    //gv_timer_send_cloud->setSingleShot(true);
    connect(gv_timer_send_cloud, SIGNAL(timeout()), this, SLOT(message_to_server()));

    //----------------------------------------------------------------------------------------------------------------
    // Timer que envia los datos segun un tiempo en el port recovery                                          [QTimer]
    //----------------------------------------------------------------------------------------------------------------
    gv_timer_send_cloud_reco = new QTimer();
    //gv_timer_send_cloud_reco->setInterval(def_delay_send_message);
    //gv_timer_send_cloud_reco->setSingleShot(true);
    connect(gv_timer_send_cloud_reco, SIGNAL(timeout()), this, SLOT(message_to_server_reco()));

    //----------------------------------------------------------------------------------------------------------------
    // Timer que verifica cada 30min el estado de carga de la beagle                                          [QTimer]
    //----------------------------------------------------------------------------------------------------------------
    gv_timer_level_energy = new QTimer();
    gv_timer_level_energy->setInterval(def_delay_send_node);
    connect(gv_timer_level_energy, SIGNAL(timeout()), this, SLOT(read_volt_hub()));
    gv_timer_level_energy->start();
}
//******************************************************************************************************************************
// Slot que conectarar la funcion principal con la senal proveniente de verificacion de internet SIGNAL[jptethernet:restart.. SLOT]
//******************************************************************************************************************************
void jptclientcloud::restart_beagle_signal(){
    emit restart_beagle_signal_ethernet();
#ifdef _COMPILE_MESSAGE
    emit viewer_status("[!] reset BBB");
#endif
}
//******************************************************************************************************************************
// Funcion que emite la lectura del conversor analogo digital para enviar a la nube                  SIGNAL[QTimer:timeout SLOT]
//******************************************************************************************************************************
void jptclientcloud::read_volt_hub(){
    jptenergylevel * read_adc = new jptenergylevel();
    connect(read_adc, SIGNAL(value_volts_and_current(QString,QString,QString)), this, SLOT(value_volts_and_current_slot(QString,QString,QString)));
    read_adc->start();
}
//******************************************************************************************************************************
// Funcion que recibe y emite la senal proveniente del thread de lectura del A/D para el voltage     SIGNAL[QTimer:timeout SLOT]
//******************************************************************************************************************************
void jptclientcloud::value_volts_and_current_slot(QString value_1, QString value_2, QString name_node){
    emit value_volts_and_current_signal(value_1, value_2, name_node);
#ifdef _COMPILE_MESSAGE
    emit viewer_status("[<] {Volt:" + value_1 + ", Curr:" + value_2 + "}");
#endif
    bool nivel = (value_1.toInt() < def_lim_voltaje_requerido) ? false : true;
    emit status_voltage(nivel, 1);
}
//******************************************************************************************************************************
// Funcion que recibe la respuesta proveniente del servidor Listener (python)                  SIGNAL[QTcpSocket:readyRead SLOT]
//******************************************************************************************************************************
void jptclientcloud::message_from_server(){
    a_logs_counters = new logs_counters();
    if(a_buffer_data.size() > 0){
        QString lv_message_server_cloud = a_socket_client->readAll();

        if(lv_message_server_cloud == "non")
        {
            //Este proceso hace que el sistema retorne el sistema.
#ifdef _COMPILE_MESSAGE
            emit viewer_status("[<] Cloud SK:[" + QString::number(a_buffer_data.size()) + "] R: " + lv_message_server_cloud);
#endif
            //Agrego a la pila en caso de que no
            //a_buffer_data_reco.append(message_json);
            //a_buffer_data.removeFirst();
            //Time for save
            QDateTime date = QDateTime::currentDateTime();
            QString date_string = date.toString("yyyy/MM/dd hh:mm:ss");
            if(message_json!=""){
                a_logs_counters->save_count_bad_cloud(date_string, message_json);
            }
            message_json="";
        }
        else{
            //Time for save
            QDateTime date = QDateTime::currentDateTime();
            QString date_string = date.toString("yyyy/MM/dd hh:mm:ss");
            a_logs_counters->save_count_good_cloud(date_string, message_json);
            message_json="";
            a_buffer_data.removeFirst();

#ifdef _HUB_ON
#ifdef _BLINK_CLOUD
            jptblinkdata *blink = new jptblinkdata(def_cloud_data);
            blink->start();
#endif
#endif

#ifdef _COMPILE_MESSAGE
            emit viewer_status("[<] Cloud SK:[" + QString::number(a_buffer_data.size()) + "] R: " + lv_message_server_cloud);
#endif
        }

        //Si es mayor a 10 se hace evacuacion por el otro puerto
        if(a_buffer_data.size() > 20){
            if(flag_time_out_reco){
                gv_timer_send_cloud->stop();
                gv_timer_send_cloud_reco->start(3000);
                flag_time_out_reco = false;
            }
        }
    }else{
        gv_timer_send_cloud_reco->stop();
        flag_time_out = true;
    }
    counter_send =0;
    counter_send_reco =0;
}
//******************************************************************************************************************************
// Funcion que recibe la respuesta proveniente del servidor (RECOVERY) Listener (python)                  SIGNAL[QTcpSocket:readyRead SLOT]
//******************************************************************************************************************************
void jptclientcloud::message_from_server_reco(){
    a_logs_counters = new logs_counters();

    QString lv_message_server_cloud = a_socket_client_reco->readAll();

    if(lv_message_server_cloud == "non")
    {
        //Este proceso hace que el sistema retorne el sistema.
#ifdef _COMPILE_MESSAGE
        emit viewer_status("[<] Cloud Port Recovery SK:["
                           + QString::number(a_buffer_data.size()) + "] R: " + lv_message_server_cloud);
#endif
        //Time for save
        QDateTime date = QDateTime::currentDateTime();
        QString date_string = date.toString("yyyy/MM/dd hh:mm:ss");
        if(message_json!=""){
            a_logs_counters->save_count_bad_cloud(date_string, message_json);
        }
        message_json="";
    }
    if(lv_message_server_cloud == "yey"){
        //Time for save
        QDateTime date = QDateTime::currentDateTime();
        QString date_string = date.toString("yyyy/MM/dd hh:mm:ss");
        a_logs_counters->save_count_good_cloud(date_string, message_json);
        message_json="";
        for(int i=0;i<counter;i++){
            qDebug()<<"borrando";
            a_buffer_data.removeFirst();
        }


#ifdef _HUB_ON
#ifdef _BLINK_CLOUD
        jptblinkdata *blink = new jptblinkdata(def_cloud_data);
        blink->start();
#endif
#endif

#ifdef _COMPILE_MESSAGE
        emit viewer_status("[<] Cloud Port Recovery SK:[" + QString::number(a_buffer_data.size()) + "] R: " + lv_message_server_cloud);
#endif
    }
    stop_all();
}
//******************************************************************************************************************************
// Reiniciar el socket que comunica con el servidor                                                             [jptclientcloud]
//******************************************************************************************************************************
void jptclientcloud::restart_socket(){
    if(!a_socket_client->state() == QAbstractSocket::ConnectingState){
        a_socket_client->close();
        qDebug()<<a_ip_cloud<<":"<<a_port_cloud;

        a_socket_client->connectToHost(QHostAddress(a_ip_cloud), a_port_cloud);
        a_socket_client->waitForConnected(10000);

    }
    //qDebug()<<a_socket_client->state();
    if(!a_socket_client_reco->state() == QAbstractSocket::ConnectingState){
        a_socket_client->close();
        qDebug()<<a_ip_cloud<<":"<<a_port_recovery;

        a_socket_client_reco->connectToHost(QHostAddress(a_ip_cloud), a_port_recovery);
        a_socket_client_reco->waitForConnected(10000);

    }
    //caso para port recovery

    check_connect_cloud();
}
//******************************************************************************************************************************
// Verifica que el socket este bien configurado                                                                 [jptclientcloud]
//******************************************************************************************************************************
void jptclientcloud::check_connect_cloud(){

    a_status_cloud = (a_socket_client->state() == QAbstractSocket::ConnectedState) ? true : false;
    a_status_cloud_reco = (a_socket_client_reco->state() == QAbstractSocket::ConnectedState) ? true : false;
#ifdef _COMPILE_MESSAGE
    QString lv_output = (a_status_cloud) ? "[o] Cloud" : ("[*] Cloud " + a_ip_cloud + ":" + QString::number(a_port_cloud));

    QString lv_output_reco = (a_status_cloud_reco) ?
                "[o] Cloud Port Recovery" : ("[*] Cloud Port Recovery " + a_ip_cloud + ":" + QString::number(a_port_recovery));

    //se tiene eun error de reconexion, en caso de que la respuesta del lv_output sea false trataremos de reconectar una sola vez mas.
    emit viewer_status(lv_output);
    emit viewer_status(lv_output_reco);

    if(!a_status_cloud)//verifico que sea falso.
    {
        a_socket_client->close();//cierro la conexion
        a_socket_client->connectToHost(QHostAddress(a_ip_cloud), a_port_cloud);
        a_socket_client->waitForConnected(10000);

        //mensaje de reconexion al sistema local
        emit viewer_status("[*] Cloud >> Reconnecting");
    }
    if(!a_status_cloud_reco){
        a_socket_client_reco->close();//cierro la conexion
        a_socket_client_reco->connectToHost(QHostAddress(a_ip_cloud), a_port_recovery);
        a_socket_client_reco->waitForConnected(10000);

        //mensaje de reconexion al sistema local
        emit viewer_status("[*] Cloud Port Recovery>> Reconnecting");
    }
    //if the answer is true m the system doesn't do anything.
    qDebug()<<"CHECK reco aqui";
#endif
}
//******************************************************************************************************************************
// Envia un mensaje al servidor de la nube                                                                      [jptclientcloud]
//******************************************************************************************************************************
void jptclientcloud::send_message_cloud(QString message){

    a_socket_client->write(message.toLatin1().data());

#ifdef _COMPILE_MESSAGE
    emit viewer_status("[>] Cloud Send Message");
#endif
}
//******************************************************************************************************************************
// Envia un mensaje al servidor de la nube trama recovery                                                       [jptclientcloud]
//******************************************************************************************************************************
void jptclientcloud::send_message_cloud_reco(QString message){
    //qDebug()<<"MSSS :"<<message;
    a_socket_client_reco->write(message.toLatin1().data());
    qDebug()<<"MSSS LATIN:"<<message.toLatin1().data();
#ifdef _COMPILE_MESSAGE
    emit viewer_status("[>] Cloud Send Message Recovery");
#endif

}
//******************************************************************************************************************************
// apilamiento de la informacion que va para la nube                                                            [jptclientcloud]
//******************************************************************************************************************************
void jptclientcloud::stack_data(QString message){
    if(message != "null"){
        message_json = message;
        a_buffer_data.append(message);
        if(flag_time_out){
            qDebug()<<"timer";
            gv_timer_send_cloud->start(2000);
        }
    }
}


//******************************************************************************************************************************
// Envia una trama a el servidor                                                                   SIGNAL[QTimer:timeour - SLOT]
//******************************************************************************************************************************
void jptclientcloud::message_to_server(){
    flag_time_out = false;
    a_status_cloud = (a_socket_client->state() == QAbstractSocket::ConnectedState) ? true : false;

    QString lv_output = (a_status_cloud) ? "[o] Cloud" : ("[*] Cloud " + a_ip_cloud + ":" + QString::number(a_port_cloud));

    emit viewer_status(lv_output);

    if(!a_status_cloud)
        system_restart();

    if(a_status_ethernet && a_status_cloud)
        if(a_buffer_data.size() > 0){
            send_message_cloud(a_buffer_data.constFirst());
        }

    counter_send++;
    if(counter_send==10){
        system_restart();
        stop_all();
    }
}
//******************************************************************************************************************************
// Envia una trama a el servidor por el puerto recovery                                            SIGNAL[QTimer:timeour - SLOT]
//******************************************************************************************************************************
void jptclientcloud::message_to_server_reco(){
    flag_time_out_reco = false;
    a_status_cloud_reco = (a_socket_client_reco->state() == QAbstractSocket::ConnectedState) ? true : false;

    QString lv_output = (a_status_cloud_reco) ?
                "[o] Cloud Port Recovery" : ("[*] Cloud Port Recovery " + a_ip_cloud + ":" + QString::number(a_port_recovery));

    emit viewer_status(lv_output);

    if(!a_status_cloud_reco)
        system_restart();

    if(a_status_ethernet && a_status_cloud_reco){
        if(!flag_busy){
            qDebug()<<"flag false";
            flag_busy = true;
            json_total.append(json_reco);
            for (counter;counter<a_buffer_data.size();counter++) {
                json_total.append(a_buffer_data[counter]+",");
                if(counter==20){
                    json_total.append(a_buffer_data[counter]);
                    json_total.append(json_reco_final);
                    break;
                }
            }
            qDebug()<<"JSON RECOVERY :"<<json_total;
            send_message_cloud_reco(json_total);
        }
    }
    json_total.clear();
    qDebug()<<"Message to aqui";
    qDebug()<<"Valor counter "<<counter;

    //system_restart
    counter_send_reco++;
    if(counter_send_reco==10){
        system_restart();
        stop_all();
    }
}
#ifdef _COMPILE_MESSAGE
//******************************************************************************************************************************
// Visualizar el estado de la nube y ethernet          SIGNAL[jptethernet:viewer_status - SLOT] -> [jptMonitoring:viewer_status]
//******************************************************************************************************************************
void jptclientcloud::viewer_status_(QString status){
    emit viewer_status(status);
}
#endif
//******************************************************************************************************************************
// Verifica el estado del internet   SIGNAL[jptethernet:status_ethertet - SLOT] -> [jptmonitoring:status_ethernet, status_cloud]
//******************************************************************************************************************************
void jptclientcloud::status_ethernet_(bool status){
    a_status_ethernet = status;
    if(!status)
        if((!a_status_cloud) || (a_buffer_data.size() != 0))
            restart_socket();

    emit status_ethernet(status, 2);
}
//******************************************************************************************************************************
// Destructor                                                                                                   [jptclientcloud]
//******************************************************************************************************************************
jptclientcloud::~jptclientcloud(){
    a_socket_client->close();
    a_socket_client_reco->close();
}
//******************************************************************************************************************************
// Errores que provienen de abrir el servidor [Socket]                                                              [QTcpSocket]
//******************************************************************************************************************************
#ifdef _COMPILE_MESSAGE
void jptclientcloud::display_error(QAbstractSocket::SocketError socketError){
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
        emit viewer_status("[e]-[cld] Close");
        //en caso de cierre o de refused el sistema debe de emtir un restart

        break;
    case QAbstractSocket::HostNotFoundError:
        emit viewer_status("[e]-[cld] Not Found");

        break;
    case QAbstractSocket::ConnectionRefusedError:
        emit viewer_status("[e]-[cld] Refused");

        break;
    default:
        emit viewer_status("[e]-[cld] " + a_socket_client->errorString());
    }
}
#endif
//******************************************************************************************************************************
// Errores que provienen de abrir el servidor [Socket] port recovery                                                [QTcpSocket]
//******************************************************************************************************************************
#ifdef _COMPILE_MESSAGE
void jptclientcloud::display_error_reco(QAbstractSocket::SocketError socketError){
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
        emit viewer_status("[e]-[cld] Close Port Recovery");
        //en caso de cierre o de refused el sistema debe de emtir un restart

        break;
    case QAbstractSocket::HostNotFoundError:
        emit viewer_status("[e]-[cld] Not Found Close Port Recovery");

        break;
    case QAbstractSocket::ConnectionRefusedError:
        emit viewer_status("[e]-[cld] Refused Close Port Recovery");

        break;
    default:
        emit viewer_status("[e]-[cld]-[Port Recovery] " + a_socket_client_reco->errorString());
    }
}
#endif

//*********************************************************************************************************************************
//*********************************************************************************************************************************
//*********************************************************************************************************************************

void jptclientcloud::set_ip_cloud(QString ip_cloud){
    a_ip_cloud=ip_cloud;
}

void jptclientcloud::set_port_cloud(int port){
    a_port_cloud=port;
}

void jptclientcloud::set_port_recovery_cloud(int port_reco){
    a_port_recovery=port_reco;
}

void jptclientcloud::system_restart(){

    a_socket_client->close();
    a_socket_client_reco->close();
    qDebug()<<a_ip_cloud<<":"<<a_port_cloud;

    a_socket_client->connectToHost(QHostAddress(a_ip_cloud), a_port_cloud);
    a_socket_client->waitForConnected(10000);

    a_socket_client_reco->connectToHost(QHostAddress(a_ip_cloud), a_port_recovery);
    a_socket_client_reco->waitForConnected(10000);

    qDebug()<<a_socket_client->state();
    check_connect_cloud();
}

void jptclientcloud::stop_all(){
    counter=0;
    flag_busy = false;
    gv_timer_send_cloud_reco->stop();
    flag_time_out= true;
    flag_time_out_reco = true;
    counter_send_reco =0;
    counter_send = 0;
}

void jptclientcloud::_check_model_old(){
    emit check_model_old();
}

