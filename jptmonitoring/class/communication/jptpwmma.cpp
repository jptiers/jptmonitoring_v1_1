#include "jptpwmma.h"

#define def_cicle_pwm 1000.0

int     gv_ran_p[4][2] = {{0,100},{0,100},{0,100},{0,100}};
QString gv_inf_p[4][2] = {{"P8.13","6"},{"P8.19","5"},{"P9.14","3"},{"P9.16","4"}};

//******************************************************************************************************************************
// Inicializar 4-20
//******************************************************************************************************************************
jptpwmma::jptpwmma(QObject *parent) : QObject(parent){
#ifdef _HUB_ON
    system("sudo sh -c 'echo cape-universaln > /sys/devices/bone_capemgr.9/slots'");
#endif
}
//******************************************************************************************************************************
// Actualizar el registro en la linea de 4-20
//******************************************************************************************************************************
void jptpwmma::set_value_pwm(QString value, QString pin_pwm){
    int lv_pos_pwm = pin_pwm.toInt() - 1;

    double lv_factor = def_cicle_pwm / (gv_ran_p[lv_pos_pwm][def_max_ran_pwm] - gv_ran_p[lv_pos_pwm][def_min_ran_pwm]);
    int lv_value = int(value.toDouble() * lv_factor);

#ifdef _HUB_ON
    system("sudo sh -c 'echo " + QString::number(lv_value).toLatin1() + " > /sys/class/pwm/pwm" + gv_inf_p[lv_pos_pwm][def_posi_pin_pwm].toLatin1() + "/duty_ns'");
#endif
#ifdef _COMPILE_MESSAGE
    emit viewer_status("[>] 4-20");
#endif
}
//******************************************************************************************************************************
// Configurar los pines de pwm
//******************************************************************************************************************************
void jptpwmma::configure_pin_pwm(QString pin_pwm, QString range){

    QStringList lv_pwm_ran = range.split("/");
    int lv_pos_pwm = pin_pwm.toInt() - 1;

    gv_ran_p[lv_pos_pwm][def_min_ran_pwm] = lv_pwm_ran[def_min_ran_pwm].toInt();
    gv_ran_p[lv_pos_pwm][def_max_ran_pwm] = lv_pwm_ran[def_max_ran_pwm].toInt();

#ifdef _HUB_ON
    init_pin_pwm(&lv_pos_pwm);
#endif
}

//******************************************************************************************************************************
// Inicializa los pines de pwm
//******************************************************************************************************************************
#ifdef _HUB_ON
void jptpwmma::init_pin_pwm(int *pos_pin){

    QByteArray lv_latin_name = gv_inf_p[*pos_pin][def_name_pin_pwm].toLatin1();
    QByteArray lv_latin_posi = gv_inf_p[*pos_pin][def_posi_pin_pwm].toLatin1();

    system("sudo sh -c 'config-pin " + lv_latin_name + " pwm'");
    system("sudo sh -c 'echo " + lv_latin_posi + " > /sys/class/pwm/export'");
    system("sudo sh -c 'echo 1000 > /sys/class/pwm/pwm" + lv_latin_posi + "/period_ns'");
    system("sudo sh -c 'echo 1 > /sys/class/pwm/pwm" + lv_latin_posi + "/run'");
}
#endif
