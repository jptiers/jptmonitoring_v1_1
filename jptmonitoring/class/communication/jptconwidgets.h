#ifndef JPTCONWIDGETS_H
#define JPTCONWIDGETS_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QJsonDocument>
#include <QJsonObject>

#include "_sentence_.h"

#define def_ip_widgets_communication    "127.0.0.1"
#define def_port_widgets_communication  10000
#define def_num_max_connections         10

class jptconwidgets : public QObject{
    Q_OBJECT
public:
    explicit jptconwidgets(QObject *parent = nullptr);
    QStringList return_list_values();


signals:
    void data_from_widgets(QString json);
#ifdef _COMPILE_MESSAGE
    void viewer_status(QString status);
#endif
    void serial_data_new(QStringList data_serial);

private:
    QTcpServer *a_server_wd;

public slots:
    void new_connection();
    void message_from_widget();
    void disconnected();
    void send_message_to_widget(QStringList name_widget, QString name_node, QString value_1, QString value_2);
    void send_message_to_widget_new_frame(QString json_frame, QString name_widget);
    void send_message_to_widget_raw(QString name_node, QString value_1, QString value_2);
};

#endif // JPTCONWIDGETS_H
