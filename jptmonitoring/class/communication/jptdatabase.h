#ifndef JPTDATABASE_H
#define JPTDATABASE_H

#include <QObject>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QString>
#include <QStringList>
#include <QVector>
#include <QVariant>

#include "_sentence_.h"
#include "../logs/logs_counters.h"


#define def_pos_table_func  0
#define def_pos_table_conf  1
#define def_pos_table_char  2
#define def_pos_table_modu  3
#define def_pos_table_pwms  4
#define def_pos_table_dela  5

#define def_num_colums_table_func 10
#define def_num_colums_table_conf 25
#define def_num_colums_table_char 23 // +2 fact (Ya sumados)
#define def_num_colums_table_modu 2
#define def_num_colums_table_pwms 9
#define def_num_colums_table_dela 4

class jptdatabase : public QObject{
    Q_OBJECT
public:
    explicit jptdatabase(QString name_data_base, QString password, QString user_name, QObject *parent = nullptr);
    ~jptdatabase();
    void update_register(QString name_table, QString table_values);
    void update_register(QString name_table, QString id, QString query);

    void get_values      (int pos_name_table, QVector<QStringList > *vec_data);
    void init_database_connection();


    void update_port_cloud(QString port_cloud);
    void update_ip_cloud(QString ip_cloud);
    void update_hub_name(QString hub_name);
    //retorna la lista de los modulos de la base de datos
    QStringList get_modules_from_db();

    void set_date_time(QString time);
    QString get_date_time();
    void set_json_trame(QString json);
    QString get_json_trame();
    QString public_json;

    //Get MODBUS confg
    QStringList get_modbus_conf();
    //Update conf modbus
    void set_modbus_conf(QString parity, QString baud, QString data_bits, QString stop_bits, QString port, QString ip, QString port_ip);
    //Get Table modbus
    QStringList get_data_widget_modbus(QString m_device);
    //Update Table modbus
    void set_table_modbus(QString date, QString data_table);
    //Clean database
    QString clean_database();

public slots:
    void insert_register (QString name_table, QString table_values);


signals:
#ifdef _COMPILE_MESSAGE
    void viewer_status(QString status);
#endif

private:
    QSqlDatabase db;
    logs_counters *a_logs_counters;
    QString datetime;
    QString json_trame;
};

#endif // JPTDATABASE_H
