#include "jptconwidgets.h"

#define def_status_client_not_identifity false
#define def_status_client_yes_identifity true

QVector<QTcpSocket *> gv_clients;
QVector<bool>         gv_status_clients;
QStringList           gv_name_widget;
bool gv_conection     = true;

//******************************************************************************************************************************
// Funcion principal                                                                                             [jptconwidgets]
//******************************************************************************************************************************
jptconwidgets::jptconwidgets(QObject *parent) : QObject(parent){
    a_server_wd = new QTcpServer(this);
    a_server_wd->listen(QHostAddress(def_ip_widgets_communication), def_port_widgets_communication);
    a_server_wd->setMaxPendingConnections(def_num_max_connections);

    connect(a_server_wd, SIGNAL(newConnection()), this, SLOT(new_connection()));
}
//******************************************************************************************************************************
// Escucha una nueva coneccion presente                                                  SIGNAL[QTcpServer:newConnection - SLOT]
//******************************************************************************************************************************
void jptconwidgets::new_connection(){
    if(gv_conection){
        gv_clients.append(a_server_wd->nextPendingConnection());
        gv_status_clients.append(def_status_client_not_identifity);
        int lv_pos_client = gv_clients.size() - 1;
        connect(gv_clients[lv_pos_client], SIGNAL(readyRead()), this, SLOT(message_from_widget()));
        connect(gv_clients[lv_pos_client], SIGNAL(disconnected()), this, SLOT(disconnected()));
        gv_conection = false;
    }else{
        QTcpSocket *socket = a_server_wd->nextPendingConnection();
        socket->close();
        socket->deleteLater();
    }
}
//******************************************************************************************************************************
// Esc ucha cuando una conexion ha sido cerrada                                             SIGNAL[QTcpSocket:disconnected- SLOT]
//******************************************************************************************************************************
void jptconwidgets::disconnected(){
    for (int f_num_client = 0; f_num_client < gv_clients.size(); ++f_num_client)
        if(gv_clients[f_num_client]->state() == QAbstractSocket::UnconnectedState){
#ifdef _COMPILE_MESSAGE
            emit viewer_status("[*] Widget: " + gv_name_widget[f_num_client]);
#endif
            gv_clients.remove(f_num_client);
            gv_status_clients.remove(f_num_client);
            gv_name_widget.removeAt(f_num_client);
        }
}
//******************************************************************************************************************************
// Transmite los valores crudos                                           SIGNAL[jptmonitoring:send_message_to_widget - SLOT]
//******************************************************************************************************************************
void jptconwidgets::send_message_to_widget_raw(QString name_node, QString value_1, QString value_2){
    for(int f_num_client = 0; f_num_client < gv_clients.size(); ++f_num_client)
        if(gv_status_clients[f_num_client])
            if(gv_name_widget[f_num_client] == "Viewer"){
                QString lv_message_widget = "raw-" + name_node + "-" + value_1 + "-" + value_2 + "-##";
                gv_clients[f_num_client]->write(lv_message_widget.toLatin1());
                gv_clients[f_num_client]->waitForBytesWritten(-1);
#ifdef _COMPILE_MESSAGE
                emit viewer_status("[>] Message to " + gv_name_widget[f_num_client]);
#endif
            }

}

void jptconwidgets::send_message_to_widget_new_frame(QString json_frame,QString name_widget){
    for(int f_num_client = 0; f_num_client < gv_clients.size(); ++f_num_client)
    {
        if(gv_status_clients[f_num_client])//verifica si el estado esta activo
        {
            if(gv_name_widget[f_num_client] == name_widget){
                gv_clients[f_num_client]->write(json_frame.toLatin1());
                gv_clients[f_num_client]->waitForBytesWritten(-1);

                emit viewer_status("[>] Message to " + gv_name_widget[f_num_client]);
            }
        }
    }
}
//******************************************************************************************************************************
// Transmite los datos al widget                                             SIGNAL[jptmonitoring:send_message_to_widget - SLOT]
//******************************************************************************************************************************
// En el instalador definir para los nodos y decir para que wiget van, con un QCombobox que defina el widget, con lo cual se
// enviara el nombre del widget al cual esta asignado ese nodo en especifico, y en la trama que contiene nombre de los sensores
// emisores de los datos; seleccionar esa tarjeta para el widget destino.
//******************************************************************************************************************************
void jptconwidgets::send_message_to_widget(QStringList name_widget, QString name_node, QString value_1, QString value_2){
    QString lv_message_widget;

    for(int f_num_client = 0; f_num_client < gv_clients.size(); ++f_num_client)
        if(gv_status_clients[f_num_client]){
            if(gv_name_widget[f_num_client] == "Viewer")
                lv_message_widget = "ing-" + name_node + "-" + value_1 + "-" + value_2 + "-##";
            else
                lv_message_widget = name_node + "-" + value_1 + "-" + value_2 + "-##";
            foreach(const QString f_name_widget, name_widget)
                if(gv_name_widget[f_num_client] == f_name_widget){
                    gv_clients[f_num_client]->write(lv_message_widget.toLatin1());
                    gv_clients[f_num_client]->waitForBytesWritten(-1);
#ifdef _COMPILE_MESSAGE
                    emit viewer_status("[>] Message to " + gv_name_widget[f_num_client]);
#endif
                }
        }

}


//******************************************************************************************************************************
// Recibe un mensaje del widget que se desea conectar                                                            [jptconwidgets]
//******************************************************************************************************************************
void jptconwidgets::message_from_widget(){
    QString lv_message;
    qDebug()<<"gv "<<gv_clients.size();
    for(int f_num_client = 0; f_num_client < gv_clients.size() ; ++f_num_client){
        if(gv_clients[f_num_client]->bytesAvailable() != 0){
            if(!gv_status_clients[f_num_client]){
                lv_message = gv_clients[f_num_client]->readAll();

                QJsonDocument lv_message_json = QJsonDocument::fromJson(lv_message.toUtf8());
                QJsonObject   lv_trame_json   = lv_message_json.object();

                if(lv_trame_json["mode"].toString() == "Start"){
                    gv_name_widget.append(lv_trame_json["name"].toString());
#ifdef _COMPILE_MESSAGE
                    emit viewer_status("[o] Widget: " + gv_name_widget[f_num_client]);
#endif
                    gv_clients[f_num_client]->write("Start");
                    gv_clients[f_num_client]->waitForBytesWritten(-1);

                    gv_status_clients[f_num_client] = def_status_client_yes_identifity;
                    gv_conection = true;
                }
            }else{

                //esta senstencia se habilita solo cuando el sistema ya tiene un widget conectado //en este caso el valor de gv_status[x]=true
                //nota: para el if de aarriba el valor es de false. por eso se regristra un nuevo elemento.
                lv_message = gv_clients[f_num_client]->readAll();
                // qDebug()<<lv_message;

                QJsonDocument lv_message_json = QJsonDocument::fromJson(lv_message.toUtf8());//obtengo el documento json
                QJsonObject   lv_trame_json   = lv_message_json.object();//obtengo el objeto json.
                QJsonObject  lv_device=lv_trame_json.value("device").toObject();
                QString  lv_device_name=lv_device.value("device_name").toString();

                qDebug()<<"Acabo de obtener una trama proveniente de..."<<lv_device_name;
                //ahora procedo a validar si este elemento existe y esta  conectado.. aunque por ovias razones si lo esta.
                qDebug()<<gv_name_widget[f_num_client]<<" //// "<<lv_device_name;

                emit viewer_status("[<] Message from " + lv_device_name);
                emit data_from_widgets(lv_message);
                /*
                //Esta sentencia funcionaba solo para la trama antigua.
                //En la nueva trama el sistema buscara el id_device y verificara si tiene uno con el mismo nombre conectado.

                QStringList lv_data_message;

                if(lv_trame_json["mode"].toString() == "Data"){
#ifdef _FRAG_BITS_NAME_NODE

#else
                    lv_data_message << lv_trame_json["node"].toString();
                    lv_data_message << lv_trame_json["value_1"].toString();
                    lv_data_message << lv_trame_json["value_2"].toString();
#endif
                    emit serial_data_new(lv_data_message);

#ifdef _COMPILE_MESSAGE
                    emit viewer_status("[<] Message from " + lv_trame_json["name"].toString());
#endif
                }*/





            }
        }
    }
}


//******************************************************************************************************************************
// retorna el listado de los nombres de los widgets                                                            [jptconwidgets]
//******************************************************************************************************************************

QStringList jptconwidgets::return_list_values()
{
    return gv_name_widget;
}
