#ifndef JPTBLINKDATA_H
#define JPTBLINKDATA_H

#define def_modbus_data 0
#define def_serial_data 1
#define def_cloud_data  2
#define def_420_data    3

#include <QThread>
#include <_sentence_.h>

class jptblinkdata : public QThread{
    Q_OBJECT

public:
    jptblinkdata(int type);
    void run();

private:
    int a_type;
    void blink_modbus();
    void blink_420();
    void blink_cloud();
    void blink_serial();
};

#endif // JPTBLINKDATA_H
