#ifndef JPTBTNEXPORT_H
#define JPTBTNEXPORT_H

#include <QThread>
#include <QProcess>

#define def_status_exporting true
#define def_status_none      false

#include "_sentence_.h"

class jptbtnexport : public QThread{
    Q_OBJECT

public:
    jptbtnexport();
    void run();

private:
    bool a_btn_status_export = def_status_exporting;

signals:
    void export_db_memory();
};

#endif // JPTBTNEXPORT_H
