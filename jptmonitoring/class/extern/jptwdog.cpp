#include "jptwdog.h"

#define def_on_pwm      true
#define def_blk_pwm     false

#define def_time_low_wd  5000
#define def_time_high_wd 100


bool state_pwm = def_on_pwm;

jptwdog::jptwdog(){}

void jptwdog::run(){
    for(;;)
        if(state_pwm == def_on_pwm)
            pwm_pin();
}

void jptwdog::reset_network(){
    state_pwm = def_blk_pwm;
}

void jptwdog::pwm_pin(){
#ifdef _HUB_ON    
    system(def_pin_wd_eth_on);
    msleep(def_time_high_wd);
    system(def_pin_wd_eth_off);
    msleep(def_time_low_wd);
#endif
}
