#include "jptenergylevel.h"

//AIN0 P9_39

//******************************************************************************************************************************
// Inicializa el conversor analogo digital
//******************************************************************************************************************************
jptenergylevel::jptenergylevel(){}
//******************************************************************************************************************************
// Funcion
//******************************************************************************************************************************
void jptenergylevel::run(){
    read_adc();
}

//******************************************************************************************************************************
// Realiza la lectura del conversor analogo Digital ADC
//******************************************************************************************************************************
void jptenergylevel::read_adc(){
    QString  lv_raw_volts = "0";

#ifdef _HUB_ON
    QProcess lv_volt;
    lv_volt.start("cat /sys/bus/iio/devices/iio:device0/in_voltage0_raw");
    lv_volt.waitForFinished(-1);
    lv_raw_volts = lv_volt.readAll();
    lv_volt.close();
#endif
    lv_raw_volts = QString::number((lv_raw_volts.toDouble() * 12.0)/4095.0);

    emit value_volts_and_current(lv_raw_volts, lv_raw_volts, "0000");
    jptenergylevel::exit();
}


