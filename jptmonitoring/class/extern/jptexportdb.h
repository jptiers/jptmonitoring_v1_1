#ifndef JPTEXPORTDB_H
#define JPTEXPORTDB_H

#include <QThread>
#include "_sentence_.h"

class jptexportdb : public QThread{
         Q_OBJECT

public:
    jptexportdb(QString a_name_hub);
    void run();

signals:
#ifdef _COMPILE_MESSAGE
    void viewer_status(QString status);
#endif

private:
    QString name_hub;

};

#endif // JPTEXPORTDB_H
