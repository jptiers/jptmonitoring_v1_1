#ifndef JPTENERGYLEVEL_H
#define JPTENERGYLEVEL_H


#include "_sentence_.h"
#include <QThread>
#include <QProcess>


class jptenergylevel : public QThread{
    Q_OBJECT

private:
    void read_adc();

signals:
    void value_volts_and_current(QString value_1, QString value_2, QString name_node);

public:
    jptenergylevel();
    void run();
};

#endif // JPTENERGYLEVEL_H
