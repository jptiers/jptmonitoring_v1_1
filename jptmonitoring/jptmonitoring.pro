#-------------------------------------------------
#
# Project created by QtCreator 2018-01-03T23:01:19
#
#-------------------------------------------------

QT       += core gui serialport sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = jptmonitoring
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#unix: INCLUDEPATH += "/jpt/dep/include"
#unix: LIBS += -L/jpt/dep/lib

SOURCES += \
    class/logs/jptthreadcounters.cpp \
    class/logs/logs_counters.cpp \
    interfaces/jptalertdialog.cpp \
    main.cpp \
    jptmonitoring.cpp \
    interfaces/jptserialdevices.cpp \
    class/communication/jptserial.cpp \
    class/communication/jptdatabase.cpp \
    class/communication/jptclientcloud.cpp \
    class/communication/jptconwidgets.cpp \
    class/extern/jptexportdb.cpp \
    class/extern/jptbtnexport.cpp \
    class/extern/jptethernet.cpp \
    class/jptabsvar.cpp \
    class/expression/except.cpp \
    class/expression/expr.cpp \
    class/expression/func.cpp \
    class/expression/funclist.cpp \
    class/expression/node.cpp \
    class/expression/parser.cpp \
    class/expression/vallist.cpp \
    class/extern/jptblinkdata.cpp \
    class/extern/jptenergylevel.cpp \
    class/communication/jptpwmma.cpp \
    class/extern/jptwdog.cpp \
    class/communication/jptdevices.cpp \
    test.cpp

HEADERS += \
    _sentence_.h \
    class/logs/jptthreadcounters.h \
    class/logs/logs_counters.h \
    interfaces/jptalertdialog.h \
    interfaces/jptalertidialog.h \
    jptmonitoring.h \
    interfaces/jptserialdevices.h \
    class/communication/jptserial.h \
    class/communication/jptdatabase.h \
    class/jptabsvar.h \
    class/expression/defs.h \
    class/expression/except.h \
    class/expression/expr.h \
    class/expression/expreval.h \
    class/expression/funclist.h \
    class/expression/node.h \
    class/expression/parser.h \
    class/expression/vallist.h \
    class/extern/jptexportdb.h \
    class/extern/jptbtnexport.h \
    class/extern/jptethernet.h \
    class/communication/jptclientcloud.h \
    class/communication/jptconwidgets.h \
    class/extern/jptblinkdata.h \
    class/extern/jptenergylevel.h \
    class/communication/jptpwmma.h \
    class/extern/jptwdog.h \
    class/communication/jptdevices.h \
    test.h

FORMS += \
    interfaces/jptalertdialog.ui \
    interfaces/jptserialdevices.ui \
        jptmonitoring.ui

RESOURCES += \
    interfaces/settings.qrc
