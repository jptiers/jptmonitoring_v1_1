#include "test.h"

test::test()
{

}



void test::generateList(){
    int ran = qrand() % ((1000 + 1) - 1) + 1;
    QString json_to_send1="{\"device\":{\"device_id\":\"0\",\"board_id\": \"00600001\",\"device_name\": \"level-temperature\",\"sensor_count\": \"9\", "
                          "\"sensors\":{";
    json_to_send1=json_to_send1+"\"1-ph\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send1=json_to_send1+"\"2-resistivity-1\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send1=json_to_send1+"\"3-resistivity-2\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send1=json_to_send1+"\"4-temperature-1\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send1=json_to_send1+"\"5-temperature-2\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send1=json_to_send1+"\"6-temperature-3\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send1=json_to_send1+"\"7-methane\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"}}}}";

    json<<json_to_send1;

    QString json_to_send2="{\"device\":{\"device_id\":\"0\",\"board_id\": \"00600002\",\"device_name\": \"level-temperature\",\"sensor_count\": \"9\", "
                          "\"sensors\":{";
    json_to_send2=json_to_send2+"\"1-ph\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send2=json_to_send2+"\"2-resistivity-1\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send2=json_to_send2+"\"3-resistivity-2\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send2=json_to_send2+"\"4-temperature-1\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send2=json_to_send2+"\"5-methane\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"}}}}";

    json<<json_to_send2;

    QString json_to_send3="{\"device\":{\"device_id\":\"0\",\"board_id\": \"00600003\",\"device_name\": \"ground-threshold\",\"sensor_count\": \"9\", "
                          "\"sensors\":{";
    json_to_send3=json_to_send3+"\"1-precipitation-accum\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send3=json_to_send3+"\"2-Acc-X\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send3=json_to_send3+"\"3-resistivity-2\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send3=json_to_send3+"\"4-methane\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"}}}}";

    json<<json_to_send3;

    QString json_to_send4="{\"device\":{\"device_id\":\"0\",\"board_id\": \"00600004\",\"device_name\": \"pluviometer\",\"sensor_count\": \"9\", "
                          "\"sensors\":{";
    json_to_send4=json_to_send4+"\"1-ph\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send4=json_to_send4+"\"2-resistivity-1\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send4=json_to_send4+"\"3-methane\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"}}}}";

    json<<json_to_send4;

    QString json_to_send5="{\"device\":{\"device_id\":\"0\",\"board_id\": \"00600005\",\"device_name\": \"level-temperature\",\"sensor_count\": \"9\", "
                          "\"sensors\":{";
    json_to_send5=json_to_send5+"\"1-ph\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send5=json_to_send5+"\"2-resistivity-1\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send5=json_to_send5+"\"3-resistivity-2\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send5=json_to_send5+"\"4-temperature-1\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send5=json_to_send5+"\"5-temperature-2\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send5=json_to_send5+"\"6-temperature-3\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send5=json_to_send5+"\"7-methane\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"}}}}";

    json<<json_to_send5;

    QString json_to_send6="{\"device\":{\"device_id\":\"0\",\"board_id\": \"00600006\",\"device_name\": \"level-temperature\",\"sensor_count\": \"9\", "
                          "\"sensors\":{";
    json_to_send6=json_to_send6+"\"1-ph\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send6=json_to_send6+"\"2-resistivity-1\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send6=json_to_send6+"\"3-resistivity-2\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send6=json_to_send6+"\"4-temperature-1\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send6=json_to_send6+"\"5-temperature-2\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send6=json_to_send6+"\"6-temperature-3\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send6=json_to_send6+"\"7-methane\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"}}}}";

    json<<json_to_send6;

    QString json_to_send7="{\"device\":{\"device_id\":\"0\",\"board_id\": \"00600007\",\"device_name\": \"level-temperature\",\"sensor_count\": \"9\", "
                          "\"sensors\":{";
    json_to_send7=json_to_send7+"\"1-ph\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send7=json_to_send7+"\"2-resistivity-1\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send7=json_to_send7+"\"3-resistivity-2\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send7=json_to_send7+"\"4-temperature-1\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send7=json_to_send7+"\"5-temperature-2\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send7=json_to_send7+"\"6-temperature-3\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send7=json_to_send7+"\"7-methane\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"}}}}";

    json<<json_to_send7;

    QString json_to_send8="{\"device\":{\"device_id\":\"0\",\"board_id\": \"00600008\",\"device_name\": \"level-temperature\",\"sensor_count\": \"9\", "
                          "\"sensors\":{";
    json_to_send8=json_to_send8+"\"1-ph\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send8=json_to_send8+"\"2-resistivity-1\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send8=json_to_send8+"\"3-resistivity-2\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send8=json_to_send8+"\"4-temperature-1\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send8=json_to_send8+"\"5-temperature-2\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send8=json_to_send8+"\"6-temperature-3\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send8=json_to_send8+"\"7-methane\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"}}}}";

    json<<json_to_send8;

    QString json_to_send9="{\"device\":{\"device_id\":\"0\",\"board_id\": \"00600009\",\"device_name\": \"level-temperature\",\"sensor_count\": \"9\", "
                          "\"sensors\":{";
    json_to_send9=json_to_send9+"\"1-ph\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send9=json_to_send9+"\"2-resistivity-1\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send9=json_to_send9+"\"3-resistivity-2\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send9=json_to_send9+"\"4-temperature-1\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send9=json_to_send9+"\"5-temperature-2\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send9=json_to_send9+"\"6-temperature-3\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send9=json_to_send9+"\"7-methane\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"}}}}";

    json<<json_to_send9;

    QString json_to_send10="{\"device\":{\"device_id\":\"0\",\"board_id\": \"006000011\",\"device_name\": \"level-temperature\",\"sensor_count\": \"9\", "
                           "\"sensors\":{";
    json_to_send10=json_to_send10+"\"1-ph\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send10=json_to_send10+"\"2-resistivity-1\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send10=json_to_send10+"\"3-resistivity-2\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send10=json_to_send10+"\"4-temperature-1\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send10=json_to_send10+"\"5-temperature-2\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send10=json_to_send10+"\"6-temperature-3\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send10=json_to_send10+"\"7-methane\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"}}}}";

    json<<json_to_send10;

    QString json_to_send11="{\"device\":{\"device_id\":\"0\",\"board_id\": \"006000012\",\"device_name\": \"level-temperature\",\"sensor_count\": \"9\", "
                           "\"sensors\":{";
    json_to_send11=json_to_send11+"\"1-ph\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send11=json_to_send11+"\"2-resistivity-1\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send11=json_to_send11+"\"3-resistivity-2\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send11=json_to_send11+"\"4-temperature-1\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send11=json_to_send11+"\"5-temperature-2\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send11=json_to_send11+"\"6-temperature-3\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"},";
    json_to_send11=json_to_send11+"\"7-methane\": {\"RAW\": \""+QString::number(ran)+"\",\"ENG\": \""+QString::number(ran)+"\"}}}}";

    json<<json_to_send11;

}

QString test::getWidget(int i){
    return json[i];
}
