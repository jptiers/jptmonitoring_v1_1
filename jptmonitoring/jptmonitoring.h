#ifndef JPTMONITORING_H
#define JPTMONITORING_H

#include <QtWidgets/QWidget>

#include <QCheckBox>


QT_BEGIN_NAMESPACE
class QComboBox;
class QCheckBox;
QT_END_NAMESPACE





//*************************************************************
// Definiciones sobre la tabla de modulos
//*************************************************************
//name_module(0)
//*************************************************************
#define def_pos_modu_name_module 0
//*************************************************************
// Definiciones sobre la tabla de Funciones
//*************************************************************
//node_id(0)
//sensor_1(1),transfer_s1(2),transfer_m1(3),transfer_c1(4)
//sensor_2(5),transfer_s2(6),transfer_m2(7),transfer_c2(8)
//*************************************************************
#define def_pos_db_function_S1_S   2
#define def_pos_db_function_S1_M   3
#define def_pos_db_function_S1_C   4

#define def_pos_db_function_S2_S   6
#define def_pos_db_function_S2_M   7
#define def_pos_db_function_S2_C   8
//*************************************************************
// Definiciones sobre la tabla de configuraciones
//*************************************************************
//hub_name(0),num_nodes(1),
//ip_server(2),port_server(3),internet(4)
//password(5)
//mdb_flag(6),mdb_id(7),mdb_type(8),mdb_parity(9),mdb_baud(10),mdb_dbits(11),mdb_sbits(12),mdb_timeout(13),mdb_tries(14)
//fac_modem(15),fac_radio(16),fac_mdb(17)
//sta_cld(18)
//mdb_input_reg(19),mdb_holding_reg(20),mdb_input_dis(21),mdb_coil_dis(22)
//sta_pwm(23)
//*************************************************************
#define def_pos_db_name_hub           0
#define def_pos_db_nume_node          1

#define def_pos_conf_db_ip_cloud      2
#define def_pos_conf_db_port_cloud    3
#define def_pos_conf_db_internet      4

#define def_pos_conf_db_password      5

#define def_pos_conf_db_status_modbus 6
#define def_pos_conf_db_id_modbus     7
#define def_pos_conf_db_type_modbus   8

#define def_pos_conf_db_baud_parity   9
#define def_pos_conf_db_baud_rate     10
#define def_pos_conf_db_data_bits     11
#define def_pos_conf_db_stop_bits     12
#define def_pos_conf_db_time_out      13
#define def_pos_conf_db_tries         14

#define def_pos_conf_db_fact_modem    15
#define def_pos_conf_db_fact_radio    16
#define def_pos_conf_db_fact_modbus   17

#define def_pos_conf_db_status_cloud  18

#define def_pos_conf_db_mdb_input_register   19
#define def_pos_conf_db_mdb_holding_register 20
#define def_pos_conf_db_mdb_input_discrete   21
#define def_pos_conf_db_mdb_coil_discrete    22

#define def_pos_conf_db_status_pwm           23
//*************************************************************
// Definiciones sobre la tabla de caracteristicas
//*************************************************************
//node_id(0)
//name_var_1(1) ,unit_1(2) ,range_1(3)
//mdb_V1(4) ,mdb_D1(5), fact_mdb_1(*6),
//cloud_1(6)
//pwm_V1(7) ,pwm_D1(8) ,pwm_range1(9)
//name_var_2(10),unit_2(11),range_2(12)
//mdb_V2(13),mdb_D2(14), fact_mdb_2(*15)
//cloud_2(15),
//pwm_V2(16),pwm_D2(17),pwm_range2(18)
//widgets(19)
//*************************************************************
#define def_pos_char_db_node_id       0

#define def_pos_char_db_name_var_1    1
#define def_pos_char_db_unit_var_1    2
#define def_pos_char_db_rang_var_1    3
#define def_pos_char_db_mdbs_var_1    4
#define def_pos_char_db_mdbs_id_var_1 5
//#define def_pos_char_db_mdb_fact_1    6 //*
#define def_pos_char_db_cloud_var_1   6
#define def_pos_char_db_pwm_var_1     7
#define def_pos_char_db_pwm_pin_var_1 8
#define def_pos_char_db_pwm_ran_var_1 9//

#define def_pos_char_db_name_var_2    10//
#define def_pos_char_db_unit_var_2    11
#define def_pos_char_db_rang_var_2    12
#define def_pos_char_db_mdbs_var_2    13
#define def_pos_char_db_mdbs_id_var_2 14
//#define def_pos_char_db_mdb_fact_2    15 //*
#define def_pos_char_db_cloud_var_2   15
#define def_pos_char_db_pwm_var_2     16
#define def_pos_char_db_pwm_pin_var_2 17
#define def_pos_char_db_pwm_ran_var_2 18
#define def_pos_char_db_widgets       19
//*************************************************************

#include <QDebug>

#include "class/communication/jptserial.h"
#include "class/jptabsvar.h"
#include "class/communication/jptdatabase.h"
#include "class/extern/jptbtnexport.h"
#include "class/extern/jptexportdb.h"
#include "class/communication/jptclientcloud.h"
#include "class/communication/jptconwidgets.h"
//#include "class/communication/modbus/jptmdbslave.h"
#include "class/communication/jptpwmma.h"
#include "_sentence_.h"
#include "class/extern/jptwdog.h"
#include "class/logs/jptthreadcounters.h"
#include "class/logs/logs_counters.h"
#include "interfaces/jptalertdialog.h"
#include "class/extern/jptethernet.h"
//#include "test.h"

#ifdef _COMPILE_GUI
    #include "interfaces/jptserialdevices.h"
    #include "ui_jptmonitoring.h"

    #include <QMainWindow>
    #include <QGuiApplication>
    #include <QScreen>
    #include <QRect>
    #include <QColor>
    #include <QLineEdit>

    #include <QLCDNumber>
    #include <QGridLayout>
    #include <QLabel>
    #include <QProcess>
#else
    #include <QObject>
#endif

#ifdef _TRAME_CLOUD_JSON
    #include <QJsonObject>
    #include <QJsonDocument>
#endif

const QString gv_user_data_base("JPT");
const QString gv_name_data_base("jptdatabase.db");
const QString gv_pass_data_base("jptconsultingandservices"); // Leer la contrasena de la base de datos desde un archivo encryptado con GPG, su contrasena es la que se muestra aqui [leer en el bucle principal]

// Definicion de las posiciones de cada parametro dentro de la trama proveniente de los radios
#define def_pos_rd_name_node 0
#ifdef _FRAG_BITS_NAME_NODE
    #define def_pos_rd_name_hubs   1
    #define def_pos_rd_value_1     2
    #define def_pos_rd_value_2     3
#else
    #define def_pos_rd_value_1   1
    #define def_pos_rd_value_2   2
#endif

#ifdef _COMPILE_GUI
namespace Ui {
class jptmonitoring;
}

class jptmonitoring : public QMainWindow{
    Q_OBJECT

public:
    explicit jptmonitoring(QWidget *parent = 0);
    void charge_widgets_modbus();
    void exe_modbus_script();

#else
class jptmonitoring : public QObject{
    Q_OBJECT

public:
    explicit jptmonitoring(QObject *parent = 0);
#endif
    ~jptmonitoring();

public:
    void save_file_wvdial(QString tty);

signals:
   void send_message_to_cloud(QString message);
   void send_message_to_modbus(const int &value, const int id, const int factor);
   void send_message_to_widget(QStringList name_widget, QString name_node, QString value_1, QString value_2);
   void send_message_to_widget_raw(QString name_node, QString value_1, QString value_2);
   void send_message_to_pwm(QString value, QString pin_pwm);
   void send_message_to_database(QString name_table, QString query);
   void send_state_hub(int value, int id);

   void send_message_to_widget_v22(QString name_node);
   void send_message_alert(QString label, bool type);

public slots:
   void test_json();

#ifdef _COMPILE_GUI
    void serial_devices_selection(QStringList serial_device);
    void gui_open_serial();
    void gui_open_alert();
    //void process();
    void calibrate_equation_btn();
    void state_protected(int dato);
    void state_protected_modbus(int dato);
    void state_protected_modbus_tcp(int dato);
    void state_protected_pass(int dato);


#endif
    void export_db_memory();
    void viewer_data(QString data);

#ifdef _COMPILE_MESSAGE
    void viewer_status(QString status);

#endif
    void serial_data_new(QStringList data_serial);
    void serial_data_reciver(int id_equipo,int id_target,int num_sensor,QList<int> sensores);
    void data_from_devices(QString trame_time,QString my_device,QString msg_cabecera,QString msg_labels,QString msg_raw_data,QString msg_ing_data,QString data_cloud_database);
    void data_from_widget(QString json);
    void build_and_send_trame_to_connections(QString value_1, QString value_2, QString name_node);

    void charge_devices_functions(QString data_txt);
    void update_devices_functions(QString _data);
    void charge_functions_modify(QString my_int);
    void save_txt_device();

    void charge_setings();
    void update_settings();
    void send_trame_json_counters(int good_db, int bad_bd, int good_cloud, int bad_cloud);
    //Modbus slots
    void generate_query_modbus();
    void click_table_modbus(int nRow, int nCol);
    void test_modbus_table();
    void update_conf_modbus();
    void generate_table_modbus();
    void change_serial_read();
    void change_serial_readwrite();
    void select_comu_modbus_rtu();
    void select_comu_modbus_tcp();
    void select_read_serial(QString serial);
    void response_alert(bool res);
    void save_table_modbus();
    void read_table_modbus();
    void stop_modbus();

    void _check_modem_old();
private:

    //comunicacion serial del sistema

    QVector<jptdevices *> lista_equipos;
    jptdevices        *consulta_equipos;

    jptserial       *a_serial_device;
    jptdatabase     *a_data_base;
    jptbtnexport    *a_watch_btn_export;
    jptclientcloud  *a_watch_status_cloud;
    //jptmdbslave     *a_watch_status_modbus;
    jptconwidgets   *a_watch_status_widget;
    jptpwmma        *a_watch_status_pwm;
    jptwdog         *a_watch_dog;
    //Add the class logs_counters
    jptthreadcounters   *a_thread_counters;
    logs_counters *a_logs_counters;
    //jptethernet *a_ethernet;

    //test *a_test;



#ifdef _COMPILE_GUI
    Ui::jptmonitoring *ui;
    //-------------------------------------------------------------------------------------------------
    // Objetos de las librerias que se implementaron
    //-------------------------------------------------------------------------------------------------
    jptserialdevices *gui_serial;
    jptalertdialog *gui_alert;

    //Headers para la tabla modbus.
    enum Columna{
      CHECK, SENSORS, VALUES, DECIMALS
    };

#ifdef _PROVE_EQUATION
    bool prove_equation(QString equation);
#endif

#endif
#ifdef _COMPILE_GUI
void generate_var_of_db(int *num_node, QString name_var_1, QString name_var_2, QString node_id);
#else
void generate_var_of_db(int *num_node, QString name_var_1, QString name_var_2);
#endif

#ifdef _TRAME_CLOUD_JSON
    QJsonObject a_trame_json;
#endif

    QString build_trame_cloud(QString value_1, QString value_2, QString name_node, QString date);


    void configure_cloud();
#ifdef _HUB_ON
    void configure_420();
    void configure_gpio();
#ifdef _CLK_UPDATE
    void sincronize_clock();
#endif
#endif
};

#endif // JPTMONITORING_H
