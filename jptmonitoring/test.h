#ifndef TEST_H
#define TEST_H

#include <QObject>

class test
{
public:
    test();
    void generateList();
    QString getWidget(int i);

    QStringList json;
};

#endif // TEST_H
