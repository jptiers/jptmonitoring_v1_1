#include "jptmonitoring.h"
#include <QCheckBox>
#include <QFormLayout>
#include <QUrl>
#include <QVector>
//Libreriras para el proces de graficacion de CHARTS

#define def_if_connected_to_cloud           if(gv_data_configu[0][def_pos_conf_db_status_cloud].toInt())
#define def_if_connected_to_modbus          if(gv_data_configu[0][def_pos_conf_db_status_modbus].toInt())
#define def_if_connected_to_pwm             if(gv_data_configu[0][def_pos_conf_db_status_pwm].toInt())
//#define def_if_connected_to_widget          if(gv_data_modules.length() > 0)
//#define def_if_connected_to_cloud_or_modbus if(gv_data_configu[0][def_pos_conf_db_status_modbus].toInt() || gv_data_configu[0][def_pos_conf_db_status_cloud].toInt())

#ifdef _COMPILE_GUI
QVector<QLCDNumber  *> gv_gui_raw_data;
QVector<QLCDNumber  *> gv_gui_ing_data;
QVector<QLineEdit   *> gv_equ_field;

QVector<QLabel  *> gv_gui_name_client;
QVector<QLabel  *> gv_gui_status_client;
QVector<QCheckBox *> gv_gui_send_client;
QVector<QLineEdit  *> gv_gui_ip_client;

// Mantener el control de despliegue de Widgets en el GridLayout.
int gv_count_rows(1);
int gv_count_cols(1);
int gv_count_node(0);

// Para realizar X columnas de Widgets.
int def_num_display_x_row_1(1);
int def_num_display_x_row_2(0);
#endif

// Contabilizar la cantidad de nodos que se destinaron.
int gv_num_nodes(0);

// Calcula los valores de ingenieria de los sensores, a partir del dato crudos.
QVector<jptabsvar  *>   gv_abs_sensor;

// Almacena la informacion proveniente de la base de datos.
QVector<QStringList >   gv_data_functio;
QVector<QStringList >   gv_data_configu; // [1] Como estos son de configuracion se puede reducir su tamano o volverlo una variable local y parametrizar el codigo con la informacion que este contiene.
QVector<QStringList >   gv_data_charact; //
QVector<QStringList >   gv_data_modules; // lo mismo que [1].
QVector<QStringList >   gv_data_pwms;    // lo mismo que [1].
QVector<QStringList >   gv_data_delayer; // lo mismo que [1].

QVector<QStringList  > gv_info_widgets;
QStringListModel* cbmodel;
QStringList db_widgets;
QStringList widgets_device;
QStringList widgets_datatime;

//Modbus
QComboBox *combo;
QCheckBox *check;
QVector<QCheckBox *> vector_modbus;
QVector<QComboBox *> vector_decimals;
bool flag_modbus=false;
QStringList table_modbus;
QProcess *program = new QProcess();
QProcess *wv_dial = new QProcess();
bool flag_modbus_active = false;
bool rtu_modbus=false;
bool tcp_ip_modbus=true;
bool flag_tty=true;
int counter_modem = 0;
//password clean db
const QString password_clean = "jptcleandatabase";

int _i = 0;

//******************************************************************************************************************************
// Funcion principal                                                                                             [jptmonitoring]
//******************************************************************************************************************************
jptmonitoring::jptmonitoring(QWidget *parent) : QMainWindow(parent), ui(new Ui::jptmonitoring){
    ui->setupUi(this);
    //Port reco
    ui->port_recovery->setText("6002");
    ui->port_recovery->setEnabled(false);
    //ESTE ES PARA HACER PRUEBAS DE TRAMAS
    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(test_json()));

#ifdef _HUB_ON
#ifdef _CLK_UPDATE
    //----------------------------------------------------------------------------------------------------------------
    // Sincroniza el reloj [RTC] con el reloj de la BBB
    //----------------------------------------------------------------------------------------------------------------
    sincronize_clock();
#endif
#endif
#ifdef _COMPILE_GUI

    cbmodel=new QStringListModel();
    ui->comboBox->setModel(cbmodel);
    connect(ui->comboBox,SIGNAL(currentTextChanged(QString)),this,SLOT(update_devices_functions(QString)));
    connect(ui->btn_save_config,SIGNAL(clicked()),this,SLOT(save_txt_device()));

    //----------------------------------------------------------------------------------------------------------------
    // Conectar Botones de la interfaz
    //----------------------------------------------------------------------------------------------------------------
    // Actualizar las ecuaciones de calibracion en la base de datos, y en los sensores.
    //connect(ui->btn_update_equ, SIGNAL(clicked()), this, SLOT(calibrate_equation_btn()));
    // Guardar la base de datos en la memoria llamada JPT
    connect(ui->btn_export_db,  SIGNAL(clicked()), this, SLOT(export_db_memory()));

    //----------------------------------------------------------------------------------------------------------------
    // Configuracion [Adaptacion del tamano de la aplicacion al tamano del display utilizado]             [MainWindow]
    //----------------------------------------------------------------------------------------------------------------
    QScreen *screen   = QGuiApplication::primaryScreen();
    QRect  dim_screem = screen->geometry();
    this->setFixedSize(dim_screem.width(), dim_screem.height());
    this->setWindowIcon(QIcon(":/init.png"));



    //----------------------------------------------------------------------------------------------------------------
    // Configuracion de la interfaz que permite seleccionar el puerto serie del xbee-datos          [jptserialdevices]
    //----------------------------------------------------------------------------------------------------------------
    gui_serial      = new jptserialdevices(this);

    connect(gui_serial, SIGNAL(was_opened(QStringList)), this, SLOT(serial_devices_selection(QStringList)));
    connect(ui->btn_gui_serial, SIGNAL(clicked()), this, SLOT(gui_open_serial()));

    //----------------------------------------------------------------------------------------------------------------
    // Alerta de la limpieza de la BD                                                                 [jptalertdialog]
    //----------------------------------------------------------------------------------------------------------------
    gui_alert    = new jptalertdialog(this);

    connect(ui->btn_clean_db, SIGNAL(clicked()), this, SLOT(gui_open_alert()));
    connect(this, SIGNAL(send_message_alert(QString, bool)), gui_alert, SLOT(message_from_moni(QString, bool)));
    connect(gui_alert, SIGNAL(answer(bool)), this, SLOT(response_alert(bool)));
#endif

    //----------------------------------------------------------------------------------------------------------------
    // Configuracion de la base de datos y carga de valores                                              [jptdatabase]
    //----------------------------------------------------------------------------------------------------------------
    a_data_base = new jptdatabase(gv_name_data_base, gv_pass_data_base, gv_user_data_base, this);


#ifdef _COMPILE_MESSAGE
    connect(a_data_base, SIGNAL(viewer_status(QString)), this, SLOT(viewer_status(QString)));
#endif
    connect(this, SIGNAL(send_message_to_database(QString,QString)), a_data_base, SLOT(insert_register(QString,QString)));

    a_data_base->init_database_connection();
    qDebug()<<"database run on way";

    a_data_base->get_values(def_pos_table_conf, &gv_data_configu); // Informacion tabla Configurations
    //a_data_base->get_values(def_pos_table_char, &gv_data_charact); // Informacion tabla Characteristics
    a_data_base->get_values(def_pos_table_modu, &gv_data_modules); // Informacion tabla Modules [que reconozca que el modulo es el widget de corte .. en el widget en la trama de inicializacion agregar que se envie el nombre]
    //a_data_base->get_values(def_pos_table_pwms, &gv_data_pwms   ); // Informacion tabla Pwms
    a_data_base->get_values(def_pos_table_dela, &gv_data_delayer); // Informacion tabla Delayer

    //consultamos los datos de los modulos conectados al sistema
    db_widgets=a_data_base->get_modules_from_db();

#ifdef _HUB_ON
    //----------------------------------------------------------------------------------------------------------------
    // Configuracion del GPIO para los botones y led indicadores                                          [MainWindow]
    //----------------------------------------------------------------------------------------------------------------
    configure_gpio();
#endif

    //----------------------------------------------------------------------------------------------------------------
    // Nombre sobre la ventana principal                                                               [jptmonitoring]
    //----------------------------------------------------------------------------------------------------------------

    gv_num_nodes = gv_data_configu[0][def_pos_db_nume_node].toInt();
#ifdef _COMPILE_GUI
    setWindowTitle("JPT Monitoring V2.4: [Name Hub: " + gv_data_configu[0][def_pos_db_name_hub]+"]");
    def_num_display_x_row_1 = (int) gv_data_configu[0][def_pos_db_nume_node].toInt()/2;
    //qDebug()<<def_num_display_x_row_1<<"mi cantidad de columnas es ";
    def_num_display_x_row_2 = def_num_display_x_row_1 - 1;
#endif

    //----------------------------------------------------------------------------------------------------------------
    // Crear la comunicacion con los widgets                                                           [jptconwidgets]
    //----------------------------------------------------------------------------------------------------------------
    //def_if_connected_to_widget{

    a_watch_status_widget = new jptconwidgets(this);
#ifdef _COMPILE_MESSAGE
    connect(a_watch_status_widget, SIGNAL(viewer_status(QString)), this, SLOT(viewer_status(QString)));
#endif

    connect(a_watch_status_widget, SIGNAL(serial_data_new(QStringList)), this, SLOT(serial_data_new(QStringList)));

    connect(this, SIGNAL(send_message_to_widget(QStringList,QString,QString,QString)), a_watch_status_widget, SLOT(send_message_to_widget(QStringList,QString,QString,QString)));
    connect(this, SIGNAL(send_message_to_widget_raw(QString,QString,QString)), a_watch_status_widget, SLOT(send_message_to_widget_raw(QString,QString,QString)));
    //conexion del widget a monitoring
    connect(a_watch_status_widget, SIGNAL(data_from_widgets(QString)), this, SLOT(data_from_widget(QString)));
    //}

#ifdef _HUB_ON
    //----------------------------------------------------------------------------------------------------------------
    // Thread que vigila el GPIO que tiene asignado el boton de descarga                                [jptbtnexport]
    //----------------------------------------------------------------------------------------------------------------
    a_watch_btn_export = new jptbtnexport();
    connect(a_watch_btn_export, SIGNAL(export_db_memory()), this, SLOT(export_db_memory()));
    a_watch_btn_export->start();
#endif


    //----------------------------------------------------------------------------------------------------------------
    // Configuracion [a_serial_device: controla la comunicacion serial con xbee]                           [jptserial]
    //----------------------------------------------------------------------------------------------------------------
    a_serial_device = new jptserial(gv_data_configu[0][def_pos_conf_db_fact_radio],gv_data_configu[0][def_pos_conf_db_baud_rate].toInt(), this);

    consulta_equipos = new jptdevices();//inicio mi variable de consulta a la libreria.

    connect(a_serial_device, SIGNAL(send_trame_to_monitoring(int,int,int,QList<int>)), this, SLOT(serial_data_reciver(int,int,int,QList<int>)));

    connect(a_serial_device, SIGNAL(viewer_status(QString)), this, SLOT(viewer_status(QString)));

    connect(a_serial_device, SIGNAL(serial_data(QStringList)), this, SLOT(serial_data_new(QStringList)));

    a_serial_device->init_serial_connection();

    //----------------------------------------------------------------------------------------------------------------
    // Vigila la conexion a la nube e internet                                                        [jptclientcloud]
    //----------------------------------------------------------------------------------------------------------------
    def_if_connected_to_cloud{
        qDebug()<<"Configure Cloud";
        configure_cloud();
    }
    charge_setings();

    //Settings Monitoring
    ui->name_hub->setEnabled(false);
    ui->port_cloud->setEnabled(false);
    ui->ip_cloud->setEnabled(false);
    ui->btn_update_settings->setEnabled(false);
    ui->port_recovery->setEnabled(false);

    connect(ui->protect_config,SIGNAL(stateChanged(int)),this,SLOT(state_protected(int)));
    //Settings Modbus RTU
    ui->txt_parity->setEnabled(false);
    ui->txt_baud->setEnabled(false);
    ui->txt_byte_size->setEnabled(false);
    ui->txt_stop_byte->setEnabled(false);
    ui->txt_port->setEnabled(false);
    ui->btn_update_modbus->setEnabled(false);
    //Settings Modbus TCP/IP
    ui->text_ip_modbus->setEnabled(false);
    ui->text_port_modbus->setEnabled(false);
    ui->btn_update_modbus_tcp->setEnabled(false);

    connect(ui->protect_modbus,SIGNAL(stateChanged(int)),this,SLOT(state_protected_modbus(int)));
    connect(ui->protect_modbus_tcp,SIGNAL(stateChanged(int)),this,SLOT(state_protected_modbus_tcp(int)));
    connect(ui->check_clean_db,SIGNAL(stateChanged(int)),this,SLOT(state_protected_pass(int)));

    //Thread counters
    a_thread_counters = new jptthreadcounters();
    a_thread_counters->start();

    //Signal and slot to logs_counters
    connect(a_thread_counters, SIGNAL(send_cloud_json_counters(int,int,int,int)), this, SLOT(send_trame_json_counters(int,int,int,int)));

    //Charge wigets for modbus
    charge_widgets_modbus();
    connect(ui->btn_queryModbus, SIGNAL(clicked()), this, SLOT(generate_query_modbus()));
    connect(ui->table_modbus, SIGNAL(cellClicked(int, int)), this, SLOT(click_table_modbus(int, int)));
    connect(ui->test_modbus, SIGNAL(clicked()), this, SLOT(test_modbus_table()));
    ui->console_modbus->setReadOnly(true);
    connect(ui->btn_update_modbus, SIGNAL(clicked()), this, SLOT(update_conf_modbus()));
    connect(ui->btn_update_modbus_tcp, SIGNAL(clicked()), this, SLOT(update_conf_modbus()));
    connect(ui->create_modbus, SIGNAL(clicked()), this, SLOT(generate_table_modbus()));
    connect(ui->active_rtu, SIGNAL(pressed()), this, SLOT(select_comu_modbus_rtu()));
    connect(ui->active_tcp_ip, SIGNAL(pressed()), this, SLOT(select_comu_modbus_tcp()));
    //save table modbus
    connect(ui->btn_save_modbus, SIGNAL(clicked()), this, SLOT(save_table_modbus()));

    connect(ui->btn_stop_modbus, SIGNAL(clicked()), this, SLOT(stop_modbus()));

    ui->active_tcp_ip->setChecked(true);

    //Conexion 232 y 485
    ui->serial_readwrite->setChecked(true);
    connect(ui->serial_onlyread, SIGNAL(pressed()), this, SLOT(change_serial_read()));
    connect(ui->serial_readwrite, SIGNAL(pressed()), this, SLOT(change_serial_readwrite()));

    connect(a_serial_device, SIGNAL(send_serial_select(QString)), this, SLOT(select_read_serial(QString)));

    //Settings cleand database
    ui->line_pass_clean_db->setEnabled(false);
    ui->btn_clean_db->setEnabled(false);

    read_table_modbus();

    connect(a_watch_status_cloud, SIGNAL(check_model_old()), this, SLOT(_check_modem_old()));
    /*
    a_test = new test();
    a_test->generateList();
    */

    ui->pushButton->setHidden(true);
}
//999999999999999999999999999999999999999999999999999999999999

void jptmonitoring::test_json(){
    /*
    QString resisitvidad_1(ui->line_r_1->text());
    QString resisitvidad_2(ui->line_r_2->text());
    QString resisitvidad_3(ui->line_r_3->text());
    QString resisitvidad_4(ui->line_r_4->text());
    QString resisitvidad_5(ui->line_r_5->text());
    QString resisitvidad_6(ui->line_r_6->text());

    QString capacitancia_1(ui->line_c_1->text());
    QString capacitancia_2(ui->line_c_2->text());
    QString capacitancia_3(ui->line_c_3->text());
    QString capacitancia_4(ui->line_c_4->text());
    QString capacitancia_5(ui->line_c_5->text());
    QString capacitancia_6(ui->line_c_6->text());

    QString temperatura(ui->line_temp->text());
    QString presion(ui->line_presion->text());


    QString json_to_send="{\"device\":{\"device_id\":\"0\",\"board_id\": \"00100001\",\"device_name\": \"level-temperature\",\"sensor_count\": \"9\", "
                         "\"sensors\":{";
    json_to_send=json_to_send+"\"1-ph\": {\"RAW\": \""+QString::number(16327)+"\",\"ENG\": \""+QString::number(16327)+"\"},";
    json_to_send=json_to_send+"\"2-resistivity-1\": {\"RAW\": \""+QString::number(16345)+"\",\"ENG\": \""+QString::number(16345)+"\"},";
    json_to_send=json_to_send+"\"3-resistivity-2\": {\"RAW\": \""+QString::number(16359)+"\",\"ENG\": \""+QString::number(16359)+"\"},";
    json_to_send=json_to_send+"\"4-temperature-1\": {\"RAW\": \""+QString::number(16359)+"\",\"ENG\": \""+QString::number(16359)+"\"},";
    json_to_send=json_to_send+"\"5-temperature-2\": {\"RAW\": \""+QString::number(16336)+"\",\"ENG\": \""+QString::number(16336)+"\"},";
    json_to_send=json_to_send+"\"6-temperature-3\": {\"RAW\": \""+QString::number(16378)+"\",\"ENG\": \""+QString::number(16378)+"\"},";
    json_to_send=json_to_send+"\"7-temperature-4\": {\"RAW\": \""+QString::number(16343)+"\",\"ENG\": \""+QString::number(16343)+"\"},";
    //json_to_send=json_to_send+"\"8-alcohol\": {\"RAW\": \""+QString::number(16378)+"\",\"ENG\": \""+QString::number(16378)+"\"},";
    //json_to_send=json_to_send+"\"9-resistivity-6\": {\"RAW\": \""+QString::number(16335)+"\",\"ENG\": \""+QString::number(16335)+"\"},";

    json_to_send=json_to_send+"\"10-capacitance-4\": {\"RAW\": \""+QString::number(16377)+"\",\"ENG\": \""+QString::number(16377)+"\"},";
    json_to_send=json_to_send+"\"11-capacitance-5\": {\"RAW\": \""+QString::number(16342)+"\",\"ENG\": \""+QString::number(16342)+"\"},";
    json_to_send=json_to_send+"\"12-capacitance-6\": {\"RAW\": \""+QString::number(16377)+"\",\"ENG\": \""+QString::number(16377)+"\"},";

    json_to_send=json_to_send+"\"9-methane\": {\"RAW\": \""+QString::number(8832)+"\",\"ENG\": \""+QString::number(45)+"\"}}}}";

    if(a_test->json.count() == _i){
        _i = 0;
        a_test->generateList();
    }
    a_watch_status_widget->send_message_to_widget_new_frame(a_test->getWidget(_i),"environment");
    _i++;

    **/
}

//999999999999999999999999999999999999999999999999999999999999

//******************************************************************************************************************************
// Funcion que envia los datos a las diferentes aplicaciones que requerieran de los datos                        [jptmonitoring]
//******************************************************************************************************************************
#define def_pos_date    0,10   // Posiciones que contienen la fecha
#define def_pos_time    11,-1  // Posiciones que contienen la hora
//******************************************************************************************************************************
void jptmonitoring::charge_widgets_modbus(){
    QStringList widgets;
    widgets.append("--Select Widget--");
    widgets.append("Water Cut");
    widgets.append("Water Quality");
    ui->comboWidget->addItems(widgets);
    //Init table
    ui->table_modbus->setColumnCount(4);
    QStringList headers;
    //Sets headers in tables
    headers << "Ckeck" << "Sensors" << "Values" << "Decimals";
    ui->table_modbus->setHorizontalHeaderLabels(headers);
}

void jptmonitoring::build_and_send_trame_to_connections(QString value_1, QString value_2, QString name_node){
    int lv_post_node = name_node.toInt()  - 1;

    QDateTime lv_now = QDateTime::currentDateTime();
    QString lv_time_send = lv_now.toString("yyyy/MM/dd hh:mm:ss");

    if(lv_post_node > -1){
        emit send_message_to_database("Node_" + name_node, "(raw_1,physic_1,raw_2,physic_2,date)VALUES('" + gv_abs_sensor[lv_post_node]->a_raw_sen_1 + "','" + value_1 + "','" + gv_abs_sensor[lv_post_node]->a_raw_sen_2 + "','" + value_2 + "','" + lv_time_send + "');");

        def_if_connected_to_pwm{
            if(gv_data_charact[lv_post_node][def_pos_char_db_pwm_var_1].toInt())
                emit send_message_to_pwm(value_1, gv_data_charact[lv_post_node][def_pos_char_db_pwm_pin_var_1]);
            if(gv_data_charact[lv_post_node][def_pos_char_db_pwm_var_2].toInt())
                emit send_message_to_pwm(value_2, gv_data_charact[lv_post_node][def_pos_char_db_pwm_pin_var_2]);
        }



        emit send_message_to_widget(gv_info_widgets[lv_post_node], name_node, value_1, value_2);


    }

}

void jptmonitoring::state_protected(int dato)
{
    //verifico estado
    if(dato == 2)
    {
        ui->name_hub->setEnabled(true);
        ui->port_cloud->setEnabled(true);
        ui->ip_cloud->setEnabled(true);
        ui->btn_update_settings->setEnabled(true);
        ui->port_recovery->setEnabled(true);
    }
    else
    {
        ui->name_hub->setEnabled(false);
        ui->port_cloud->setEnabled(false);
        ui->ip_cloud->setEnabled(false);
        ui->btn_update_settings->setEnabled(false);
        ui->port_recovery->setEnabled(false);
    }
}

void jptmonitoring::state_protected_modbus(int dato)
{
    //verifico estado
    if(dato == 2)
    {
        ui->txt_parity->setEnabled(true);
        ui->txt_baud->setEnabled(true);
        ui->txt_byte_size->setEnabled(true);
        ui->txt_stop_byte->setEnabled(true);
        ui->txt_port->setEnabled(true);
        ui->btn_update_modbus->setEnabled(true);
    }
    else
    {
        ui->txt_parity->setEnabled(false);
        ui->txt_baud->setEnabled(false);
        ui->txt_byte_size->setEnabled(false);
        ui->txt_stop_byte->setEnabled(false);
        ui->txt_port->setEnabled(false);
        ui->btn_update_modbus->setEnabled(false);
    }
}

void jptmonitoring::state_protected_modbus_tcp(int dato)
{
    //verifico estado
    if(dato == 2)
    {
        ui->text_ip_modbus->setEnabled(true);
        ui->text_port_modbus->setEnabled(true);
        ui->btn_update_modbus_tcp->setEnabled(true);
    }
    else
    {
        ui->text_ip_modbus->setEnabled(false);
        ui->text_port_modbus->setEnabled(false);
        ui->btn_update_modbus_tcp->setEnabled(false);
    }
}

void jptmonitoring::state_protected_pass(int dato)
{
    //verifico estado
    if(dato == 2)
    {
        ui->line_pass_clean_db->setEnabled(true);
        ui->btn_clean_db->setEnabled(true);
    }
    else
    {
        ui->line_pass_clean_db->setEnabled(false);
        ui->btn_clean_db->setEnabled(false);
    }
}


void jptmonitoring::charge_setings()
{
    //cargamos los datos del sistema de envio
    ui->name_hub->setText(gv_data_configu[0][def_pos_db_name_hub]);
    ui->ip_cloud->setText(gv_data_configu[0][def_pos_conf_db_ip_cloud]);
    ui->port_cloud->setText(gv_data_configu[0][def_pos_conf_db_port_cloud]);
    //Misma ip pero diferente puerto de recovery
    ui->port_recovery->setText("6002");

    //creo la conexion del sistema
    connect(ui->btn_update_settings,SIGNAL(clicked()),this,SLOT(update_settings()));

    //Iniciamos valores del modbus
    QStringList config_modbus;
    config_modbus = a_data_base->get_modbus_conf();
    ui->txt_parity->setText(config_modbus[0]);
    ui->txt_baud->setText(config_modbus[1]);
    ui->txt_byte_size->setText(config_modbus[2]);
    ui->txt_stop_byte->setText(config_modbus[3]);
    ui->txt_port->setText(config_modbus[4]);
    ui->text_ip_modbus->setText(config_modbus[5]);
    ui->text_port_modbus->setText(config_modbus[6]);
}

void jptmonitoring::update_settings()
{
    //sistema que actualiza los valores de envio a la nube
    //primero el sistema obtendra los valores actualies de la parte grarica.
    QString name_hub=ui->name_hub->text();
    QString ip_cloud=ui->ip_cloud->text();
    QString port_cloud=ui->port_cloud->text();

    a_data_base->update_ip_cloud(ip_cloud);
    a_data_base->update_hub_name(name_hub);
    a_data_base->update_port_cloud(port_cloud);

    //luego de este punto debemos de reiniciar los valores o sobre escribirlos
    gv_data_configu.clear();
    a_data_base->get_values(def_pos_table_conf, &gv_data_configu); // Se carga nuevamente Informacion tabla Configurations
    setWindowTitle("JPT Monitoring V2.4: [Name Hub: " + gv_data_configu[0][def_pos_db_name_hub]+"]");
    a_watch_status_cloud->set_ip_cloud(ip_cloud);
    int temp_port=port_cloud.toInt();
    a_watch_status_cloud->set_port_cloud(temp_port);
    a_watch_status_cloud->system_restart();
}

//******************************************************************************************************************************
// Funcion  encargada de cargar los datos de ip y nombre de los clientes en el sistema.
//******************************************************************************************************************************
void jptmonitoring::serial_data_reciver(int id_equipo,int id_target,int num_sensor,QList<int> sensores)
{

    qDebug()<<"recibio trama"<<id_equipo<<id_target<<num_sensor;

    //lo primero es verificar si el elemento de la lista de guardado es cero
    //si es cero debo registrar mi funcion
    if(lista_equipos.isEmpty())
    {
        //si esta vacia registro la primera trama en el sistema
        lista_equipos.append(new jptdevices());
        //lista_equipos[0]->init_device(3,id_target,num_sensor,sensores);

        connect(lista_equipos[0],SIGNAL(combo_box_element(QString)),this,SLOT(charge_devices_functions(QString)));
        lista_equipos[0]->init_device(id_equipo,id_target,num_sensor,sensores);

        qDebug()<<"Mi lista ahora tiene tamaño"<<lista_equipos.size();
        //para prueba consulto la lista de variables del sistema

        connect(lista_equipos[0],SIGNAL(signal_to_viewer_reciver(QString,QString,QString,QString,QString,QString,QString)),this,SLOT(data_from_devices(QString,QString,QString,QString,QString,QString,QString)));

        serial_data_reciver(id_equipo,id_target,num_sensor,sensores);



    }
    else
    {//si mi puntero no esta vacio el ciclo se debe de encargar de verificar si ya existe
        //otro vector o arreglo con el mismo nombre
        bool existe_dentro=false;
        int numero_dentro_vector;
        for(int i=0;i<lista_equipos.size();i++)//recorre toda la lista
        {
            if(lista_equipos[i]->get_id_targeta()==id_target)
            {
                //si pasa esto el equipo ya esta registrado en el sistema
                numero_dentro_vector=i;
                existe_dentro=true;

                break;
            }

        }
        //Nota: SOlo se hara recurividad cuando se hace registro de datos en caso contrario se carga la informaicon al sistema

        if(existe_dentro==true)
        {
            qDebug()<<"Equipo ya registrado en sistema";
            //como el equipo ya esta registrado solo actualizo el valor de los datos dentro del sistema
            lista_equipos[numero_dentro_vector]->set_sensor_raw(sensores);

            lista_equipos[numero_dentro_vector]->start();


        }
        else
        {
            //Proceso de nuevo registro del sistema
            int temp_tam=lista_equipos.size();
            lista_equipos.append(new jptdevices());
            connect(lista_equipos[temp_tam],SIGNAL(combo_box_element(QString)),this,SLOT(charge_devices_functions(QString)));

            lista_equipos[temp_tam]->init_device(id_equipo,id_target,num_sensor,sensores);
            qDebug()<<"Mi lista ahora tiene tamaño"<<lista_equipos.size();
            connect(lista_equipos[temp_tam],SIGNAL(signal_to_viewer_reciver(QString,QString,QString,QString,QString,QString,QString)),this,SLOT(data_from_devices(QString,QString,QString,QString,QString,QString,QString)));
            serial_data_reciver(id_equipo,id_target,num_sensor,sensores);

        }
    }


}

void jptmonitoring::charge_devices_functions(QString data_txt)
{
    //cargamos los dattos iniciales del sistema
    //primero agrego el elemento a la combobox
    //antes de crear el elemento verifico que el sistema no contenga uno de igual nombre
    QStringList item_list=cbmodel->stringList();
    bool exist_item=false;
    if(item_list.size()>0 )
    {

        for(int i=0;i<item_list.size();i++)
        {
            //recorro mi lista de combo box
            if(item_list[i]==data_txt)
            {
                //si esto sucede entonces el elemento ya esta en la lista
                exist_item=true;
            }
        }
    }
    if(!exist_item)
    {//se adiciona el elemento a ala lista
        item_list.append(data_txt);
        cbmodel->setStringList(item_list);
        ui->comboBox->setModel(cbmodel);



    }


}
void jptmonitoring::update_devices_functions(QString _data)
{
    //ahora parceo el valor de la data obtenida
    QStringList _list=_data.split(">>");
    //El primer elemento es el id y el segundo es el nombre
    ui->label_select_device->setText(_list[0]);
    ui->text_device->setPlainText("");
    charge_functions_modify(_list[0]);

}

void jptmonitoring::charge_functions_modify(QString my_int)
{

    //Se carga el path de conexion o almacenmiento del archivo .txt
    QString path_source ="/jpt/jptmonitoring/External/Devices/ID-"+my_int+".txt";

    QFile file(path_source);//leo el archivo si existe
    if(!file.exists())
    {
        qDebug() << "NO existe el archivo de funciones de transferencia de equipos: ID-"<<my_int;

    }
    else
    {//leo mi archivo
        if (file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QTextStream stream(&file);

            QString data=stream.readAll();

            ui->text_device->setPlainText(data);

        }
        file.close();
    }// fin del ciclo else

}

void jptmonitoring::save_txt_device()
{
    //obtengo el nombre del device
    QString name_device=ui->label_select_device->text();


    //Se carga el path de conexion o almacenmiento del archivo .txt
    QString path_source ="/jpt/jptmonitoring/External/Devices/ID-"+name_device+".txt";

    QFile file(path_source);//leo el archivo si existe
    if(!file.exists())
    {
        qDebug() << "NO existe el archivo de funciones de transferencia de equipos: ID-"<<name_device;

    }
    else
    {//leo mi archivo
        if(file.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            QTextStream datosArchivo(&file);
            datosArchivo <<ui->text_device->toPlainText()<<endl;


        }
        file.close();
    }// fin del ciclo else


    //ahora emito señal de actualizacion de funciones
    for(int i=0;i<lista_equipos.size();i++){
        QString valor_target=QString::number(lista_equipos[i]->get_id_targeta());//obtengo el valor del id de targeta
        //se compara el valor con el id de targeta que se tiene
        if(name_device==valor_target)
        {
            //si son iguales atualizo la lista de variables o funciones de correccion.
            lista_equipos[i]->update_correct_f();

        }


    }





}


void jptmonitoring::data_from_devices(QString trame_time,QString my_device,QString msg_cabecera,QString msg_labels,QString msg_raw_data,QString msg_ing_data,QString data_cloud_database)
{
    viewer_data(msg_cabecera);
    viewer_data(msg_labels);
    viewer_data("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");

    QString m_device=my_device;

    QStringList widgets_connected=a_watch_status_widget->return_list_values();

    //ahora debo validar si existe el widget y esta conectado
    bool flg_widget=false;
    int flg_position=1000;
    bool rev_environment=false;
    for(int j=0;j<db_widgets.size();j++)    {
        //recorro mi lista de widgets de la base de datos.
        if(widgets_connected.size()>0)        {
            for(int i=0;i<widgets_connected.size();i++)            {
                qDebug()<<""<<widgets_connected[i]<<"este es mi valor del widget.";
                if(db_widgets[j]==widgets_connected[i]){//si el valor es igual existe y esta conectado
                    flg_widget=true;
                    flg_position=j;
                    //qDebug()<<"Existe el widget y esta conectado";
                }
            }
        }
    }
    /*
    AQUI ES DONDE SE DISTRIBUYE LA DATA PARA LOS WIDGETSSSSSSS
    */
    if(flg_widget==true){
        //si es verdadera el widget esta conectado en el sistema y registrado en la base de datos.
        bool rev_corte=true;
        bool rev_calidad=true;
        bool rev_ferme=true;

        qDebug()<<"Existe en la validacion";
        for (int i=0;i<99;i++) {
            //para calidad
            QString conditional ="";
            if(i<10){
                conditional = "0020000";
            }else{
                conditional = "002000";
            }

            if(m_device == conditional+QString::number(i)){
                QString data_to_cloud;
                QString temp_data=":{\"hub_id\":\""+gv_data_configu[0][def_pos_db_name_hub]+"\",";
                temp_data=temp_data+"\"timestamp\":\""+trame_time+"\",";
                temp_data=temp_data+"\"device_id\"";
                data_to_cloud=data_cloud_database.replace(":{\"device_id\"",temp_data);
                a_watch_status_widget->send_message_to_widget_new_frame(data_cloud_database,db_widgets[flg_position]);
                rev_calidad=true;
                break;
            }else{
                rev_calidad=false;
            }
        }
        for (int i=0;i<99;i++) {
            //para corte
            QString conditional ="";
            if(i<10){
                conditional = "0010000";
            }else{
                conditional = "001000";
            }
            if(m_device == conditional+QString::number(i)){
                QString data_to_cloud;
                QString temp_data=":{\"hub_id\":\""+gv_data_configu[0][def_pos_db_name_hub]+"\",";
                temp_data=temp_data+"\"timestamp\":\""+trame_time+"\",";
                temp_data=temp_data+"\"device_id\"";
                data_to_cloud=data_cloud_database.replace(":{\"device_id\"",temp_data);
                a_watch_status_widget->send_message_to_widget_new_frame(data_cloud_database,db_widgets[flg_position]);
                rev_corte=true;
                break;
            }else{
                rev_corte=false;
            }
        }
        for (int i=0;i<99;i++) {
            //para fermenter
            QString conditional ="";
            if(i<10){
                conditional = "0120000";
            }else{
                conditional = "012000";
            }
            if(m_device == conditional+QString::number(i)){
                QString data_to_cloud;
                QString temp_data=":{\"hub_id\":\""+gv_data_configu[0][def_pos_db_name_hub]+"\",";
                temp_data=temp_data+"\"timestamp\":\""+trame_time+"\",";
                temp_data=temp_data+"\"device_id\"";
                data_to_cloud=data_cloud_database.replace(":{\"device_id\"",temp_data);
                a_watch_status_widget->send_message_to_widget_new_frame(data_cloud_database,db_widgets[flg_position]);
                rev_ferme=true;
                break;
            }else{
                rev_ferme=false;
            }
        }
        for (int i=0;i<999;i++) {
            //para fermenter
            QString conditional ="";
            if(i<10){
                conditional = "0060000";
            }else if(i<=10 && i>=99){
                conditional = "006000";
            }else{
                conditional = "00600";
            }
            if(m_device == conditional+QString::number(i)){
                QString data_to_cloud;
                QString temp_data=":{\"hub_id\":\""+gv_data_configu[0][def_pos_db_name_hub]+"\",";
                temp_data=temp_data+"\"timestamp\":\""+trame_time+"\",";
                temp_data=temp_data+"\"device_id\"";
                data_to_cloud=data_cloud_database.replace(":{\"device_id\"",temp_data);
                a_watch_status_widget->send_message_to_widget_new_frame(data_cloud_database,db_widgets[flg_position]);
                rev_environment=true;
                break;
            }else{
                rev_environment=false;
            }
        }

        if(!rev_corte && !rev_calidad && !rev_ferme){
            //Cuando se detecte el dispositivo se hace la insercion a la BD y al cloud
            //Primero a la BD
            QString sqlite_query="";
            sqlite_query="(m_datetime,m_device,m_data,verify_send) VALUES('"+msg_ing_data+"',";
            sqlite_query=sqlite_query+"'"+m_device+"',";
            sqlite_query=sqlite_query+"'"+data_cloud_database+"','f')";

            //from here i can get the data
            //set data for counters
            a_data_base->set_date_time(msg_ing_data);
            a_data_base->set_json_trame(data_cloud_database);
            //qDebug()<<"TRAMA PUBLIC JSON "<<a_logs_counters.public_json;
            //qDebug()<<"TRAMA JSON GET "<<a_logs_counters.get_json_trame();
            //set data for counters



            a_data_base->insert_register("Nodex",sqlite_query);

            //Enviamos la data a la nube---------------------------------------------------------------------------------------

            QString data_to_cloud="";

            //qDebug()<<trame_time<<"Data a modificar";

            //:{"device_id"

            //QString temp_data=":{\"hub_id\":\""+QString::number(id_hub)+"\",";
            QString temp_data=":{\"hub_id\":\""+gv_data_configu[0][def_pos_db_name_hub]+"\",";
            temp_data=temp_data+"\"timestamp\":\""+trame_time+"\",";
            temp_data=temp_data+"\"device_id\"";
            data_to_cloud=data_cloud_database.replace(":{\"device_id\"",temp_data);

            //qDebug()<<data_to_cloud;

            emit  send_message_to_cloud(data_to_cloud);
            qDebug()<<"ESTA ES LA DATA QUE NECESITO------- de la 645";
            //qDebug()<<data_to_cloud;
            viewer_data(data_to_cloud);
            viewer_data("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        }
    }

    if(!flg_widget || rev_environment){
        //Cuando se detecte el dispositivo se hace la insercion a la BD y al cloud
        //Primero a la BD
        QString sqlite_query="";
        sqlite_query="(m_datetime,m_device,m_data,verify_send) VALUES('"+msg_ing_data+"',";
        sqlite_query=sqlite_query+"'"+m_device+"',";
        sqlite_query=sqlite_query+"'"+data_cloud_database+"','f')";

        //from here i can get the data
        //set data for counters
        a_data_base->set_date_time(msg_ing_data);
        a_data_base->set_json_trame(data_cloud_database);
        //qDebug()<<"TRAMA PUBLIC JSON "<<a_logs_counters.public_json;
        //qDebug()<<"TRAMA JSON GET "<<a_logs_counters.get_json_trame();
        //set data for counters



        a_data_base->insert_register("Nodex",sqlite_query);

        //Enviamos la data a la nube---------------------------------------------------------------------------------------

        QString data_to_cloud="";

        //qDebug()<<trame_time<<"Data a modificar";

        //:{"device_id"

        //QString temp_data=":{\"hub_id\":\""+QString::number(id_hub)+"\",";
        QString temp_data=":{\"hub_id\":\""+gv_data_configu[0][def_pos_db_name_hub]+"\",";
        temp_data=temp_data+"\"timestamp\":\""+trame_time+"\",";
        temp_data=temp_data+"\"device_id\"";
        data_to_cloud=data_cloud_database.replace(":{\"device_id\"",temp_data);

        //qDebug()<<data_to_cloud;

        emit  send_message_to_cloud(data_to_cloud);
        qDebug()<<"ESTA ES LA DATA QUE NECESITO------- de la 645";
        //qDebug()<<data_to_cloud;
        viewer_data(data_to_cloud);
        viewer_data("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
    }
}

int contador_filas=0;
void jptmonitoring::viewer_data(QString data)
{


    ui->my_viewer_status->setTextColor(QColor(255,255,255));
    ui->my_viewer_status->append(data);

    ++contador_filas;
    if(contador_filas> 1000){
        ui->my_viewer_status->clear();
        contador_filas= 0;
    }


    qDebug() << data << "\n";

}

//******************************************************************************************************************************
// Funcion que construye la trama que se enviara al servidor nube                                                [jptmonitoring]
//******************************************************************************************************************************
QString jptmonitoring::build_trame_cloud(QString value_1, QString value_2, QString name_node, QString date){

    int lv_post_node = name_node.toInt()  - 1;


    // Se puede modificar para que en la base de datos se seleccione como se enviara la trama a la nube. Json o Normal.
#ifdef _TRAME_CLOUD_JSON
    a_trame_json["node_id"]     = name_node;
    a_trame_json["sensor_type"] = "";
    a_trame_json["value1"]      = value_1;
    a_trame_json["value2"]      = value_2;
    a_trame_json["timestamp"]   = lv_time_send;

    QJsonDocument lv_doc(a_trame_json);

    QString lv_trame_server(lv_doc.toJson());
    lv_trame_server += "!!";
    lv_trame_server.remove("\n");
    lv_trame_server.remove(" ");
    lv_trame_server.replace('"', "'");
#else

    QString lv_trame_server("&&\n0101");

    if(lv_post_node > -1){
        //if(gv_data_charact[lv_post_node][def_pos_char_db_name_var_1] == "null")
        //if(gv_data_charact[lv_post_node][def_pos_char_db_name_var_2] == "null")

        lv_trame_server += gv_data_configu[0][def_pos_db_name_hub]                  + "\n0105";
        lv_trame_server += date.mid(def_pos_date).replace("/","")                   + "CST\n0106";
        lv_trame_server += date.mid(def_pos_time)                                   + "\n0110Node";
        lv_trame_server += name_node                                                + "\n0111";
        lv_trame_server += gv_data_charact[lv_post_node][def_pos_char_db_name_var_1]+ "\n0112";
        lv_trame_server += gv_data_charact[lv_post_node][def_pos_char_db_unit_var_1]+ "\n0113";
        lv_trame_server += value_1                                                  + "\n0114Node";
        lv_trame_server += name_node                                                + "\n0115";
        lv_trame_server += gv_data_charact[lv_post_node][def_pos_char_db_name_var_2]+ "\n0116";
        lv_trame_server += gv_data_charact[lv_post_node][def_pos_char_db_unit_var_2]+ "\n0117";
        lv_trame_server += value_2                                                  + "\n!!";
    }else{
        lv_trame_server += gv_data_configu[0][def_pos_db_name_hub]                  + "\n0105";
        lv_trame_server += date.mid(def_pos_date).replace("/","")                   + "CST\n0106";
        lv_trame_server += date.mid(def_pos_time)                                   + "\n0110Node";
        lv_trame_server += name_node                                                + "\n0111";
        lv_trame_server += "Voltage\n0112";
        lv_trame_server += "Volt\n0113";
        lv_trame_server +=  value_1 + "\n0114Node";
        lv_trame_server += "0000\n0115";
        lv_trame_server += "Current\n0116";
        lv_trame_server += "Amp\n0117";
        lv_trame_server +=  value_2 + "\n!!";
    }
#endif
    return lv_trame_server;
}

#ifdef _HUB_ON
//******************************************************************************************************************************
// Funcion para configurar el GPIO de la tarjeta para BBB (botones y led externos)                                        [jptmonitoring]
//******************************************************************************************************************************
void jptmonitoring::configure_gpio(){
    //system("sudo sh -c 'echo 31 > /sys/class/gpio/export'");   //Button Copying
    system("sudo chmod 777 /sys/class/gpio");


    /*
    system("sudo sh -c 'echo 66 > /sys/class/gpio/export'");   //Signal State
    system("sudo sh -c 'echo 67 > /sys/class/gpio/export'");   //Server Conection
    system("sudo sh -c 'echo 68 > /sys/class/gpio/export'");   //USB Copying
    system("sudo sh -c 'echo 69 > /sys/class/gpio/export'");   // Reiniciar la beagle por watchdog
    */
    system("sudo chmod 777 /sys/class/gpio/gpio31/");
    system("sudo sh -c 'echo in > /sys/class/gpio/gpio31/direction'");
    system("sudo chmod 777 /sys/class/gpio/gpio66/");
    system("sudo sh -c 'echo out > /sys/class/gpio/gpio66/direction'");
    system("sudo chmod 777 /sys/class/gpio/gpio67/");
    system("sudo sh -c 'echo out > /sys/class/gpio/gpio67/direction'");
    system("sudo chmod 777 /sys/class/gpio/gpio68/");
    system("sudo sh -c 'echo out > /sys/class/gpio/gpio68/direction'");
    system("sudo chmod 777 /sys/class/gpio/gpio69/");
    system("sudo sh -c 'echo out > /sys/class/gpio/gpio69/direction'");

    //system("sudo sh -c 'echo cape-bone-iio > /sys/devices/bone_capemgr.9/slots'"); // Configuracion de los puertos analogo digital
}
#endif



//******************************************************************************************************************************
// Funciones que inicializa las variables necesarias para el programa                                            [jptmonitoring]
//******************************************************************************************************************************

//******************************************************************************************************************************
// Exportar la base de datos                SIGNAL[btn_export_db(GUI), pul_export_db(Fisico){Thread} - SLOT] -> Ejecuta QThread.
//******************************************************************************************************************************
void jptmonitoring::export_db_memory(){
    jptexportdb *lv_export_db = new jptexportdb(gv_data_configu[0][def_pos_db_name_hub]);
#ifdef _COMPILE_MESSAGE
    connect(lv_export_db, SIGNAL(viewer_status(QString)), this, SLOT(viewer_status(QString)));
#endif
    lv_export_db->start();
}
#ifdef _COMPILE_GUI
//******************************************************************************************************************************
// Cuando se cambia las ecuaciones de calibracion                                                  SIGNAL[gv_gui_btn_equ - SLOT]
//******************************************************************************************************************************
void jptmonitoring::calibrate_equation_btn(){
    for(int f_num_node = 0; f_num_node < gv_num_nodes; ++f_num_node){
        int lv_s1 = (f_num_node * 2);
        int lv_s2 = lv_s1 + 1;

        QString lv_s1_equ = gv_equ_field[lv_s1]->text();
        QString lv_s2_equ = gv_equ_field[lv_s2]->text();

        if(lv_s1_equ != gv_data_functio[f_num_node][def_pos_db_function_S1_C]){
            if(lv_s1_equ.contains("w")){
#ifdef _PROVE_EQUATION
                if(prove_equation(lv_s1_equ)){
#endif
                    gv_data_functio[f_num_node][def_pos_db_function_S1_C] = lv_s1_equ;
                    gv_abs_sensor[f_num_node]->set_correc_calib(gv_data_functio[f_num_node][def_pos_db_function_S1_C], def_sensor_1_thr);
#ifdef _PROVE_EQUATION
                    a_data_base->update_register("Functions", QString::number(f_num_node + 1), "transfer_c1='" + lv_s1_equ + "'");
                    gv_equ_field[lv_s1]->setStyleSheet("background-color: rgb(50,200,50,100)");
                }else
                    gv_equ_field[lv_s1]->setStyleSheet("background-color: rgb(200,50,50,100)");
#endif
            }else
                gv_equ_field[lv_s1]->setStyleSheet("background-color: rgb(200,50,50,100)");
        }
#ifdef _PROVE_EQUATION
        else
            gv_equ_field[lv_s1]->setStyleSheet("background-color: white");
#endif

        if(lv_s2_equ != gv_data_functio[f_num_node][def_pos_db_function_S2_C]){
            if(lv_s2_equ.contains("w")){
#ifdef _PROVE_EQUATION
                if(prove_equation(lv_s2_equ)){
#endif
                    gv_data_functio[f_num_node][def_pos_db_function_S2_C] = lv_s2_equ;
                    gv_abs_sensor[f_num_node]->set_correc_calib(gv_data_functio[f_num_node][def_pos_db_function_S2_C], def_sensor_2_thr);
#ifdef _PROVE_EQUATION
                    a_data_base->update_register("Functions", QString::number(f_num_node + 1), "transfer_c2='" + lv_s2_equ + "'");
                    gv_equ_field[lv_s2]->setStyleSheet("background-color: rgb(50,200,50,100)");
                }else
                    gv_equ_field[lv_s2]->setStyleSheet("background-color: rgb(200,50,50,100)");
#endif
            }else
                gv_equ_field[lv_s2]->setStyleSheet("background-color: rgb(200,50,50,100)");
        }
#ifdef _PROVE_EQUATION
        else
            gv_equ_field[lv_s2]->setStyleSheet("background-color: white");
#endif
    }
}
#ifdef _PROVE_EQUATION
//******************************************************************************************************************************
// Permite probar la ecuaciones que esten bien escritas                                                          [jptmoitoring]
//******************************************************************************************************************************
bool jptmonitoring::prove_equation(QString equation){
    bool validate_equ = true;
    try{
        equation.replace("w", "1");
        ValueList       lv_vlist;
        FunctionList    lv_flist;
        lv_vlist.AddDefaultValues();
        lv_flist.AddDefaultFunctions();

        Expression lv_e;
        lv_e.SetValueList(&lv_vlist);
        lv_e.SetFunctionList(&lv_flist);
        lv_e.Parse(equation.toStdString());
    }catch(...){
        validate_equ = false;
    }
    return validate_equ;
}
#endif
//******************************************************************************************************************************
// Abrir la configuracion del puerto serial                                                             [GUI - jptserialdevices]
//******************************************************************************************************************************
void jptmonitoring::gui_open_serial(){
    gui_serial->setModal(true);
    gui_serial->show();
    gui_serial->raise();
    gui_serial->activateWindow();
}
//******************************************************************************************************************************
// Alerta de limpieza de BD                                                                                             [GUI - ]
//******************************************************************************************************************************
void jptmonitoring::gui_open_alert(){
    gui_alert->setModal(true);
    gui_alert->show();
    gui_alert->raise();
    gui_alert->activateWindow();
    if(password_clean==ui->line_pass_clean_db->text()){
        emit send_message_alert("Are you sure clean database?", true);
    }else{
        emit send_message_alert("          Incorrect password", false);
    }
}

void jptmonitoring::response_alert(bool res){
    if(res){
        gui_alert->close();

        gui_alert->setModal(true);
        gui_alert->show();
        gui_alert->raise();
        gui_alert->activateWindow();

        if(a_data_base->clean_database() == "Database cleaned sucefully!"){
            emit send_message_alert("Database cleaned sucefully!", false);
            ui->line_pass_clean_db->clear();
        }

    }else{
        gui_alert->close();
    }
}
//******************************************************************************************************************************
// Cuando se selecciona un nuevo puerto serial, se cargan los parametros                  SIGNAL-SLOT   [GUI - jptserialdevices]
//******************************************************************************************************************************
void jptmonitoring::serial_devices_selection(QStringList serial_device){
    gv_data_configu[0][def_pos_conf_db_fact_modem    ] = serial_device[0];
    gv_data_configu[0][def_pos_conf_db_fact_radio    ] = serial_device[1];
    gv_data_configu[0][def_pos_conf_db_fact_modbus   ] = serial_device[2];
    gv_data_configu[0][def_pos_conf_db_baud_rate     ] = serial_device[3];
    a_serial_device->set_radio_factory_and_baud(gv_data_configu[0][def_pos_conf_db_fact_radio],gv_data_configu[0][def_pos_conf_db_baud_rate].toInt());
    a_data_base->update_register("Configurations", "mdb_baud='"+ serial_device[3] +"',fac_modem='"+ serial_device[0]+"',fac_radio='" + serial_device[1]+ "',fac_mdb='"+ serial_device[2] + "'");
}
#endif
//******************************************************************************************************************************
// Datos que se reciben del puerto serial                                                     SIGNAL-SLOT    [Radio - jptserial]
//******************************************************************************************************************************
void jptmonitoring::serial_data_new(QStringList data_serial){
    int lv_index(0);
    bool ok_v = true;
    lv_index    = (data_serial[def_pos_rd_name_node].toInt(&ok_v, 16) - 1);

#ifdef _FRAG_BITS_NAME_NODE
    int lv_name_hub(0);
    lv_name_hub = (data_serial[def_pos_rd_name_hubs].toInt());
#endif

    if(lv_index == 1)
#ifdef _COMPILE_MESSAGE
        viewer_status("[<] Serial " + data_serial[def_pos_rd_name_node]);
#endif

    if(lv_index < gv_num_nodes){
#ifdef _COMPILE_GUI
        int lv_index_par    = lv_index * 2;
        int lv_index_impar  = lv_index_par + 1;
#endif
        //cargo mis datos raw para el sensor, por ejemplo el nodo lv_index en mi hilo de datos.
        gv_abs_sensor[lv_index]->a_raw_sen_1 = data_serial[def_pos_rd_value_1];

        gv_abs_sensor[lv_index]->a_raw_sen_2 = data_serial[def_pos_rd_value_2];

#ifdef _COMPILE_GUI
        gv_gui_raw_data[lv_index_par  ]->display(data_serial[def_pos_rd_value_1]);
        gv_gui_raw_data[lv_index_impar]->display(data_serial[def_pos_rd_value_2]);
#endif
        emit send_message_to_widget_raw(QString::number(lv_index + 1), data_serial[def_pos_rd_value_1], data_serial[def_pos_rd_value_2]);
        gv_abs_sensor[lv_index]->start();

    }
}


#ifdef _COMPILE_MESSAGE
//******************************************************************************************************************************
// Visualizar el estado del programa                                                                             [jptmonitoring]
//******************************************************************************************************************************
int count_val = 0;

void jptmonitoring::viewer_status(QString status){
#ifdef _COMPILE_GUI

    ui->tx_gui_status->setTextColor(QColor(255,255,255));
    ui->tx_gui_status->append(status);


    ++count_val;
    if(count_val > 1000){
        ui->tx_gui_status->clear();
        count_val = 0;
    }

#else
    qDebug() << status << "\n";
#endif
}
#endif
//******************************************************************************************************************************
// Destructor                                                                                                    [jptmonitoring]
//******************************************************************************************************************************
jptmonitoring::~jptmonitoring(){
#ifdef _COMPILE_GUI
    delete ui;
#endif
    save_table_modbus();
    program->kill();
    program->close();
    program->terminate();

    wv_dial->kill();
    wv_dial->close();
    wv_dial->terminate();
}


//******************************************************************************************************************************
// Funcion que configura los parametros de la comunicacion con la nube                                           [jptmonitoring]
//******************************************************************************************************************************
void jptmonitoring::configure_cloud(){
    a_watch_status_cloud = new jptclientcloud(gv_data_configu[0][def_pos_conf_db_ip_cloud], gv_data_configu[0][def_pos_conf_db_port_cloud].toInt(),ui->port_recovery->text().toInt(), this);
#ifdef _COMPILE_MESSAGE
    connect(a_watch_status_cloud, SIGNAL(viewer_status(QString)), this, SLOT(viewer_status(QString)));
#endif
    connect(this, SIGNAL(send_message_to_cloud(QString)), a_watch_status_cloud, SLOT(stack_data(QString)));

    connect(a_watch_status_cloud, SIGNAL(value_volts_and_current_signal(QString,QString,QString)), this, SLOT(build_and_send_trame_to_connections(QString,QString,QString)));

    a_watch_dog = new jptwdog();
    connect(a_watch_status_cloud, SIGNAL(restart_beagle_signal_ethernet()), a_watch_dog, SLOT(reset_network()));
    a_watch_dog->start();

}
#ifdef _HUB_ON
#ifdef _CLK_UPDATE
//******************************************************************************************************************************
// Funcion que configura los parametros de la comunicacion con la nube                                           [jptmonitoring]
//******************************************************************************************************************************
void jptmonitoring::sincronize_clock(){
    system("sudo sh -c 'python /jpt/jptmonitoring/rtcupdatem.pyc'");
}
#endif
#endif

//******************************************************************************************************************************
// Slot que recibe el JSON del widget                                                                            [jptmonitoring]
//******************************************************************************************************************************
void jptmonitoring::data_from_widget(QString m_json){
    //qDebug()<<m_json<<" LLEGO AL SLOT";
    //Toda la informacion ya esta en wl widget

    //Parseamos la data para obtener el m_device

    QJsonDocument lv_message_json=QJsonDocument::fromJson(m_json.toUtf8());
    QJsonObject   lv_trama_json = lv_message_json.object();
    QJsonObject  lv_device=lv_trama_json.value("device").toObject();
    QString lv_board_id=lv_device.value("board_id").toString();
    QString lv_id_device = lv_device.value("device_id").toString();
    QString m_device("");
    m_device = lv_board_id;
    //COMO SON VARIAS TARJETAS VALIDAMOS CUAL DE TODAS PARA CERRAR EL SCRIPT
    for (int i=0;i<99;i++) {
        QString conditional ="";
        if(i<10){
            conditional = (m_device.contains('001')) ? "0010000" : "0020000";
            qDebug()<<"m_device "<<m_device;
            qDebug()<<conditional;
        }else{
            conditional = (m_device.contains('002')) ? "001000" : "002000";
        }
        QString dev = conditional+QString::number(i);
        if(m_device == dev){
            while(program->isOpen()){
                if(flag_modbus_active && rtu_modbus){
                    //program->terminate();
                    program->kill();
                }
                break;
            }
        }
    }

    //pasamos la inforamcion del json a la variable local
    QString data_cloud_database(m_json);

    //para obtener el tiempo, obtenemos el dia actual y lo convertimos a los formatos que necesitamos.
    QDateTime my_time=QDateTime::currentDateTime();

    QDateTime my_time_to_send=my_time.addSecs(18000);//tiempo mas 5horas //esto se hace dado que la hora de colombia es utc-5;

    //qDebug()<<my_time_to_send.toString("yyyy/MM/dd hh:mm:ss ")<<"Este es el tiempo que se envia ";
    //sistema para envio del timestamp utc

    my_time_to_send.setTimeSpec(Qt::UTC);

    uint utc_time=my_time_to_send.toTime_t();
    //qDebug()<<utc_time;
    QString time_to_send=QString::number(utc_time);

    qDebug()<<"Time To send UTC"<<time_to_send;
    QString m_datetime=my_time.toString("yyyy/MM/dd hh:mm:ss ");

    //Primero a la BD
    //preparamamos la consulta query
    QString sqlite_query="";
    sqlite_query="(m_datetime,m_device,m_data,verify_send) VALUES('"+m_datetime+"',";
    sqlite_query=sqlite_query+"'"+m_device+"',";
    sqlite_query=sqlite_query+"'"+data_cloud_database+"','f')";


    a_data_base->set_date_time(m_datetime);
    a_data_base->set_json_trame(data_cloud_database);

    //enviamos a la base de datos.

    a_data_base->insert_register("Nodex",sqlite_query);

    //Enviamos la data a la nube---------------------------------------------------------------------------------------

    QString data_to_cloud="";

    QString temp_data=":{\"hub_id\":\""+gv_data_configu[0][def_pos_db_name_hub]+"\",";
    temp_data=temp_data+"\"timestamp\":\""+time_to_send+"\",";
    temp_data=temp_data+"\"device_id\"";
    data_to_cloud=data_cloud_database.replace(":{\"device_id\"",temp_data);

    qDebug()<<data_to_cloud;

    //Data to be saved on cloud

    emit  send_message_to_cloud(data_to_cloud);
    qDebug()<<"ESTA ES LA DATA QUE NECESITO";
    //qDebug()<<data_to_cloud;
    viewer_data(data_to_cloud);
    viewer_data("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");

    for (int i=0;i<99;i++) {
        QString conditional ="";
        if(i<10){
            conditional = (m_device.contains('001')) ? "0010000" : "0020000";
        }else{
            conditional = (m_device.contains('002')) ? "001000" : "002000";
        }
        QString dev = conditional+QString::number(i);
        if(m_device == dev){
            if(flag_modbus_active){
                generate_query_modbus();
                test_modbus_table();
                generate_table_modbus();
                //exe_modbus_script();
                break;
            }
        }
    }
}

//******************************************************************************************************************************
// Slot to recieve logs counters                                                                                 [jptmonitoring]
//******************************************************************************************************************************
void jptmonitoring::send_trame_json_counters(int good_db, int bad_bd, int good_cloud, int bad_cloud){
    a_logs_counters = new logs_counters();
    //para obtener el tiempo, obtenemos el dia actual y lo convertimos a los formatos que necesitamos.
    QDateTime my_time=QDateTime::currentDateTime();

    QDateTime my_time_to_send=my_time.addSecs(18000);//tiempo mas 5horas //esto se hace dado que la hora de colombia es utc-5;
    my_time_to_send.setTimeSpec(Qt::UTC);

    uint utc_time=my_time_to_send.toTime_t();
    //qDebug()<<utc_time;
    QString time_to_send=QString::number(utc_time);


    QString trame_json_counters =
            "{\"counters\":"
            "{\"hub_id\":\""+gv_data_configu[0][def_pos_db_name_hub]+
            "\",\"timestamp\":\""+time_to_send+
            "\",\"counter_good_local_bd\":\""+QString::number(good_db)+"\","
                                                                       "\"counter_bad_local_bd\":\""+QString::number(bad_bd)+"\","
                                                                                                                             "\"counter_good_cloud\":\""+QString::number(good_cloud)+"\","
                                                                                                                                                                                     "\"counter_bad_cloud\":\""+QString::number(bad_cloud)+"\"}}";
    qDebug()<<"TRAMA LOGS "<<trame_json_counters;
    emit  send_message_to_cloud(trame_json_counters);
}

//******************************************************************************************************************************
// Slot to recieve widget combobox modbus (Query)                                                                [jptmonitoring]
//******************************************************************************************************************************
void jptmonitoring::generate_query_modbus(){
    QString widget("");
    widget = ui->comboWidget->currentText();
    QStringList getCut;
    QStringList getSWQ;
    QStringList decimal;

    for (int j=0;j<5;j++) {
        decimal.append(QString::number(j));
    }
    int fila;
    if(widget == "--Select Widget--"){

    }
    if(widget == "Water Cut"){

        for (int i=0;i<=99;i++) {
            QString conditional ="";
            if(i<10){
                conditional = "0010000";
            }else{
                conditional = "001000";
            }
            QString dev = conditional+QString::number(i);
            getCut = a_data_base->get_data_widget_modbus(dev);
            if(getCut.size()>0){
                break;
            }
        }
        //se lee el JSON de la base de datos

        QJsonDocument jsonCut=QJsonDocument::fromJson(getCut[2].toUtf8());
        QJsonObject trama_json = jsonCut.object();
        QJsonObject device_cut =trama_json.value("device").toObject();
        QJsonObject sensors = device_cut.value("sensors").toObject();
        //se toman los sensores del widget
        QStringList sensors_keys = sensors.keys();
        //recorremos la lista
        for (int c=0;c<sensors_keys.size();c++) {
            //obtenemos los valores de eng
            QJsonObject sensors_eng = sensors.value(sensors_keys[c]).toObject();
            QString eng = sensors_eng.value("ENG").toString();
            if(flag_modbus){
                //en caso de true solo actualizamos la columna eng
                ui->table_modbus->item(c, 2)->setText(eng);
            }else{
                //en caso de false se añaden todos los valores a sus respectivas columnas
                ui->table_modbus->insertRow(ui->table_modbus->rowCount());
                fila = ui->table_modbus->rowCount() - 1;
                combo = new QComboBox(this);
                check = new QCheckBox(this);
                combo->addItems(decimal);
                vector_modbus.append(check);
                vector_decimals.append(combo);

                //en este caso se parte la cadena de los sensores por que algunos tienen como ejem
                //estos casos 1-resi..-1
                QStringList split = sensors_keys[c].split("-");

                ui->table_modbus->setCellWidget(fila, CHECK, check);
                if(split.size() > 2){
                    QString sensor_final = split[1] +"-"+ split[2];
                    ui->table_modbus->setItem(fila, SENSORS, new QTableWidgetItem(sensor_final));
                }else{
                    QString sensor_final = split[1];
                    ui->table_modbus->setItem(fila, SENSORS, new QTableWidgetItem(sensor_final));
                }
                //se agregan el resto de los items
                ui->table_modbus->setItem(fila, VALUES, new QTableWidgetItem(eng));
                ui->table_modbus->setCellWidget(fila, DECIMALS, combo);
            }
        }
        flag_modbus = true;
    }
    if(widget == "Water Quality"){
        for (int i=0;i<=99;i++) {
            QString conditional ="";
            if(i<10){
                conditional = "0020000";
            }else{
                conditional = "002000";
            }
            QString dev = conditional+QString::number(i);
            getSWQ = a_data_base->get_data_widget_modbus(dev);
            if(getSWQ.size()>0){
                break;
            }
        }
        //se lee el JSON de la base de datos

        QJsonDocument jsonSWQ=QJsonDocument::fromJson(getSWQ[2].toUtf8());
        QJsonObject trama_json = jsonSWQ.object();
        QJsonObject device_cut =trama_json.value("device").toObject();
        QJsonObject sensors = device_cut.value("sensors").toObject();
        //se toman los sensores del widget
        QStringList sensors_keys = sensors.keys();
        //recorremos la lista
        for (int c=0;c<sensors_keys.size();c++) {
            //obtenemos los valores de eng
            QJsonObject sensors_eng = sensors.value(sensors_keys[c]).toObject();
            QString eng = sensors_eng.value("ENG").toString();
            if(flag_modbus){
                //en caso de true solo actualizamos la columna eng
                ui->table_modbus->item(c, 2)->setText(eng);
            }else{
                //en caso de false se añaden todos los valores a sus respectivas columnas
                ui->table_modbus->insertRow(ui->table_modbus->rowCount());
                fila = ui->table_modbus->rowCount() - 1;
                combo = new QComboBox(this);
                check = new QCheckBox(this);
                combo->addItems(decimal);
                vector_modbus.append(check);
                vector_decimals.append(combo);

                //en este caso se parte la cadena de los sensores por que algunos tienen como ejem
                //estos casos 1-resi..-1
                QStringList split = sensors_keys[c].split("-");

                ui->table_modbus->setCellWidget(fila, CHECK, check);
                if(split.size() > 2){
                    QString sensor_final = split[1] +"-"+ split[2];
                    ui->table_modbus->setItem(fila, SENSORS, new QTableWidgetItem(sensor_final));
                }else{
                    QString sensor_final = split[1];
                    ui->table_modbus->setItem(fila, SENSORS, new QTableWidgetItem(sensor_final));
                }
                //se agregan el resto de los items
                ui->table_modbus->setItem(fila, VALUES, new QTableWidgetItem(eng));
                ui->table_modbus->setCellWidget(fila, DECIMALS, combo);
            }
        }
        flag_modbus = true;
    }
}

//******************************************************************************************************************************
// Slots to recieve type communication modbus                                                                     [jptmonitoring]
//******************************************************************************************************************************
void jptmonitoring::select_comu_modbus_rtu(){
    if(ui->active_rtu->isCheckable()){
        rtu_modbus=true;
        tcp_ip_modbus =false;
        ui->active_tcp_ip->setChecked(false);
    }
}

void jptmonitoring::select_comu_modbus_tcp(){
    if(ui->active_tcp_ip->isCheckable()){
        tcp_ip_modbus =true;
        rtu_modbus = false;
        ui->active_rtu->setChecked(false);
    }
}

void jptmonitoring::click_table_modbus(int nRow, int nCol){
    qDebug()<<"Row "<<nRow;
    qDebug()<<"Col "<<nCol;
}

void jptmonitoring::test_modbus_table(){
    ui->console_modbus->setText("");
    table_modbus.clear();
    for (int h=0;h<vector_modbus.size();h++) {
        //DESPUES DE SABER LA POSICION DEL QUE ESTA CHECKEADO SE DETERMINA CON ESE VALOR LA FILA Y SE LLAMA LOS VALORES
        if(vector_modbus[h]->isChecked()){
            if(vector_decimals[h]->currentText() == "0"){
                //Valor entero
                int value = ui->table_modbus->item(h,2)->text().toInt();
                QString texto = ui->table_modbus->item(h,1)->text() + "=" + QString::number(value);
                ui->console_modbus->append(texto);
                table_modbus.append(texto);
            }
            if(vector_decimals[h]->currentText() == "1"){
                QStringList value_list;
                //Valor float
                float value = ui->table_modbus->item(h,2)->text().toFloat();
                //qDebug()<<"value "<<value;
                //Parseo de decimales
                QString value_format = QString::number(value, 'f',1);
                //qDebug()<<"value format"<<value_format;
                //Sensor por ahora 1 high y 1 low
                value_list = value_format.split(".");
                //Seteo a la consola
                QString texto = ui->table_modbus->item(h,1)->text() + "_1H=" + value_list[0];
                QString texto1 = ui->table_modbus->item(h,1)->text() + "_1L=" + value_list[1];
                ui->console_modbus->append(texto);
                ui->console_modbus->append(texto1);
                table_modbus.append(texto);
                table_modbus.append(texto1);
            }
            if(vector_decimals[h]->currentText() == "2"){
                QStringList value_list;
                //Valor float
                float value = ui->table_modbus->item(h,2)->text().toFloat();
                //qDebug()<<"value "<<value;
                //Parseo de decimales
                QString value_format = QString::number(value, 'f',2);
                //qDebug()<<"value format"<<value_format;
                //Sensor por ahora 1 high y 1 low
                value_list = value_format.split(".");
                //Seteo a la consola
                QString texto = ui->table_modbus->item(h,1)->text() + "_1H=" + value_list[0];
                QString texto1 = ui->table_modbus->item(h,1)->text() + "_1L=" + value_list[1];
                ui->console_modbus->append(texto);
                ui->console_modbus->append(texto1);
                table_modbus.append(texto);
                table_modbus.append(texto1);
            }
            if(vector_decimals[h]->currentText() == "3"){
                QStringList value_list;
                //Valor float
                float value = ui->table_modbus->item(h,2)->text().toFloat();
                //qDebug()<<"value "<<value;
                //Parseo de decimales
                QString value_format = QString::number(value, 'f',3);
                //qDebug()<<"value format"<<value_format;
                //Sensor por ahora 1 high y 1 low
                value_list = value_format.split(".");
                //Seteo a la consola
                QString texto = ui->table_modbus->item(h,1)->text() + "_1H=" + value_list[0];
                QString texto1 = ui->table_modbus->item(h,1)->text() + "_1L=" + value_list[1];
                ui->console_modbus->append(texto);
                ui->console_modbus->append(texto1);
                table_modbus.append(texto);
                table_modbus.append(texto1);
            }
            if(vector_decimals[h]->currentText() == "4"){
                QStringList value_list;
                //Valor float
                float value = ui->table_modbus->item(h,2)->text().toFloat();
                //qDebug()<<"value "<<value;
                //Parseo de decimales
                QString value_format = QString::number(value, 'f',4);
                //qDebug()<<"value format"<<value_format;
                //Sensor por ahora 1 high y 1 low
                value_list = value_format.split(".");
                //Seteo a la consola
                QString texto = ui->table_modbus->item(h,1)->text() + "_1H=" + value_list[0];
                QString texto1 = ui->table_modbus->item(h,1)->text() + "_1L=" + value_list[1];
                ui->console_modbus->append(texto);
                ui->console_modbus->append(texto1);
                table_modbus.append(texto);
                table_modbus.append(texto1);
            }
        }
    }
    qDebug()<<"Tabla modbus "<<table_modbus;
}

void jptmonitoring::update_conf_modbus(){
    QString paridad = ui->txt_parity->text();
    QString baud = ui->txt_baud->text();
    QString byte_size = ui->txt_byte_size->text();
    QString stop = ui->txt_stop_byte->text();
    QString port = ui->txt_port->text();
    QString ip = ui->text_ip_modbus->text();
    QString port_ip = ui->text_port_modbus->text();
    a_data_base->set_modbus_conf(paridad,baud,byte_size,stop,port,ip,port_ip);
}

void jptmonitoring::generate_table_modbus(){
    //para obtener el tiempo, obtenemos el dia actual y lo convertimos a los formatos que necesitamos.
    QDateTime my_time=QDateTime::currentDateTime();

    QDateTime my_time_to_send=my_time.addSecs(18000);//tiempo mas 5horas //esto se hace dado que la hora de colombia es utc-5;

    //qDebug()<<my_time_to_send.toString("yyyy/MM/dd hh:mm:ss ")<<"Este es el tiempo que se envia ";
    //sistema para envio del timestamp utc

    my_time_to_send.setTimeSpec(Qt::UTC);

    uint utc_time=my_time_to_send.toTime_t();
    //qDebug()<<utc_time;
    QString time_to_send=QString::number(utc_time);

    QString m_datetime=my_time.toString("yyyy/MM/dd hh:mm:ss");

    QString header_json = "[{\"table_modbus\":[";
    for (int i=0;i<table_modbus.size();i++) {
        QStringList table_split = table_modbus[i].split("=");

        if(i == table_modbus.size()-1){
            header_json.append("{\"name\":\""+table_split[0]+
                    "\",\"value\":\""+table_split[1]+"\"}]}]");
        }else{
            header_json.append("{\"name\":\""+table_split[0]+
                    "\",\"value\":\""+table_split[1]+"\"}"+",");
        }
    }
    a_data_base->set_table_modbus(m_datetime, header_json);

    viewer_status("[o] Modbus Table Created");
    qDebug()<<"TRAMA MODBUS "<<header_json;

    exe_modbus_script();

}

void jptmonitoring::exe_modbus_script(){
    if(rtu_modbus){
        while (program->isOpen()) {
            program->kill();
            program->close();
            program->terminate();
            qDebug()<<"Kill exe ";
        }
        program->setProgram("python");
        program->setArguments(QStringList() <<"/jpt/jptmonitoring/External/modbusRTU.py");
        program->start();
    }
    if(tcp_ip_modbus){
        if(program->isOpen()){

        }else{
            while(!program->isOpen()){
                qDebug()<<"tcp";
                program->setProgram("python3");
                program->setArguments(QStringList() <<"/jpt/jptmonitoring/External/modbusTCP.py");
                program->start();
            }
        }
    }
    flag_modbus_active = true;
    qDebug()<<"OPEN MODBUS";
}

//Seleccion de tipo de comunicacion
void jptmonitoring::change_serial_read(){
    //a_serial_device->close_port();
    a_serial_device->set_serial_only_read(true);
}

void jptmonitoring::change_serial_readwrite(){
    //a_serial_device->close_port();
    a_serial_device->set_serial_only_read(false);
}

void jptmonitoring::select_read_serial(QString serial){
    qDebug()<<serial;
    if(serial == "485"){
        ui->serial_onlyread->setChecked(false);
        ui->serial_readwrite->setChecked(true);
        a_serial_device->set_serial_only_read(false);
    }
    if(serial == "232"){
        ui->serial_readwrite->setChecked(false);
        ui->serial_onlyread->setChecked(true);
        a_serial_device->set_serial_only_read(true);
    }
}

void jptmonitoring::save_table_modbus(){
    if(ui->comboWidget->currentText() == "Water Quality"){
        QStringList sensors_list;
        sensors_list << "PH" << "Salinity" << "Dissolved Oxygen" << "TSS 180°" << "TSS 90°"
                     << "Temperature" << "Oil/Water" << "Conductivity-AC" << "Conductivity-DC"
                     << "WQI";
        //Create route where this located the file
        QString path = "/jpt/jptmonitoring/External/modbus_table.txt";
        QString path1 = "/jpt/jptmonitoring/External/modbus_tool.txt";
        //Evaluate if exist file
        QFile file(path);
        QFile file1(path1);
        //Apply in the conditional
        if(!file.exists()){
            //If not exist file, create one
            qDebug()<<"SE CREO ARCHIVO CONFIG";
            QFile create_file(path);
            if(create_file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)){
                QTextStream input_data(&create_file);
                //GUARDA LA TABLA MODBUS
                if(vector_modbus.count()!= 0 && vector_decimals.count()!= 0){
                    for(int i=0; i<sensors_list.count(); i++){
                        int check = vector_modbus[i]->isChecked() ? 1:0;
                        QString decimal = vector_decimals[i]->currentText();
                        input_data<<sensors_list[i]<<"||"<<check<<"||"<<decimal<<endl;

                    }
                }

            }
            create_file.close();
        }else{
            if(file.open(QIODevice::ReadWrite | QIODevice::Truncate | QIODevice::Text)){
                QTextStream input_data(&file);
                if(vector_modbus.count()!= 0 && vector_decimals.count()!= 0){
                    for(int i=0; i<sensors_list.count(); i++){
                        int check = vector_modbus[i]->isChecked() ? 1:0;
                        QString decimal = vector_decimals[i]->currentText();
                        input_data<<sensors_list[i]<<"||"<<check<<"||"<<decimal<<endl;
                    }
                }
            }
            file.close();
        }

        if(!file1.exists()){
            //If not exist file, create one
            qDebug()<<"SE CREO ARCHIVO CONFIG";
            QFile create_file(path1);
            if(create_file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)){
                QTextStream input_data(&create_file);
                //GUARDA LA TABLA MODBUS
                input_data<<"TOOL||"<<ui->comboWidget->currentText();

            }
            create_file.close();
        }else{
            if(file1.open(QIODevice::ReadWrite | QIODevice::Truncate | QIODevice::Text)){
                QTextStream input_data(&file);
                input_data<<"TOOL||"<<ui->comboWidget->currentText();
            }
            file1.close();
        }
    }
}

void jptmonitoring::read_table_modbus(){
    QString path = "/jpt/jptmonitoring/External/modbus_table.txt";
    QString path1 = "/jpt/jptmonitoring/External/modbus_tool.txt";
    //Evaluate if exist file
    int i=0;
    int fila;
    QStringList sensors;
    QStringList _check;
    QStringList decimals;

    QStringList decimal;

    for (int j=0;j<5;j++) {
        decimal.append(QString::number(j));
    }

    QFile file1(path1);
    if(file1.exists()){
        if(file1.open(QIODevice::ReadOnly)) {
            QTextStream in(&file1);
            while(!in.atEnd()){
                QString line = in.readLine();//cada linea lee
                QStringList split = line.split("||");//parte cadena
                ui->comboWidget->setCurrentText(split[1]);
            }
        }
    }

    QFile file(path);
    if(file.exists()){
        if(file.open(QIODevice::ReadOnly)) {
            QTextStream in(&file);
            while(!in.atEnd()){
                QString line = in.readLine();//cada linea lee
                QStringList split = line.split("||");//parte cadena
                sensors<<split[0];
                _check<<split[1];
                decimals<<split[2];

                i++;//aumenta
            }

            for (int c=0;c<sensors.size();c++) {
                //obtenemos los valores de eng
                if(flag_modbus){
                    //en caso de true solo actualizamos la columna eng
                    ui->table_modbus->item(c, 2)->setText(0);
                }else{
                    //en caso de false se añaden todos los valores a sus respectivas columnas
                    ui->table_modbus->insertRow(ui->table_modbus->rowCount());
                    fila = ui->table_modbus->rowCount() - 1;
                    combo = new QComboBox(this);
                    check = new QCheckBox(this);
                    //check al combo
                    _check[c] == "1" ? check->setChecked(true):check->setChecked(false);
                    combo->addItems(decimal);
                    combo->setCurrentText(decimals[c]);
                    vector_modbus.append(check);
                    vector_decimals.append(combo);

                    ui->table_modbus->setCellWidget(fila, CHECK, check);

                    ui->table_modbus->setItem(fila, SENSORS, new QTableWidgetItem(sensors[c]));

                    //se agregan el resto de los items
                    ui->table_modbus->setItem(fila, VALUES, new QTableWidgetItem("0"));
                    ui->table_modbus->setCellWidget(fila, DECIMALS, combo);
                }
            }
            flag_modbus = true;

            generate_query_modbus();
            test_modbus_table();
            generate_table_modbus();

        }
    }
}

void jptmonitoring::stop_modbus(){
    if(rtu_modbus){
        while (program->isOpen()) {
            program->kill();
            program->close();
            program->terminate();
            qDebug()<<"Kill exe ";
        }
    }
    if(tcp_ip_modbus){
        if(program->isOpen()){
            program->kill();
            program->close();
            program->terminate();
            qDebug()<<"Kill exe ";
        }
    }
    flag_modbus_active = false;
    viewer_status("[x] Close Modbus");
    qDebug()<<"CLOSE MODBUS";
}

void jptmonitoring::save_file_wvdial(QString tty){
    system("sudo rm -r /etc/wvdial.conf");
    system("sudo cp /jpt/jptmonitoring/External/wvdial.conf /etc/wvdial.conf");
    system("sudo chmod 777 /etc/wvdial.conf");

    if(flag_tty){
        QFile file("/etc/wvdial.conf");
        file.open(QIODevice::ReadOnly);

        QString str_config_ethernet;
        str_config_ethernet = file.readAll();
        file.close();
        str_config_ethernet.replace("port",  "/dev/ttyUSB0");
        file.open(QIODevice::WriteOnly);
        QTextStream sv(&file);
        sv << str_config_ethernet;
        file.close();
    }else{
        QFile file("/etc/wvdial.conf");
        file.open(QIODevice::ReadOnly);

        QString str_config_ethernet;
        str_config_ethernet = file.readAll();
        file.close();
        str_config_ethernet.replace("port", "/dev/ttyUSB1");
        file.open(QIODevice::WriteOnly);
        QTextStream sv(&file);
        sv << str_config_ethernet;
        file.close();
    }


}

void jptmonitoring::_check_modem_old(){
    qDebug("aqui llego modem");
    qDebug()<< counter_modem;
    if(ui->check_modem_old->isChecked()){
        if(wv_dial->isOpen() && counter_modem >= 16){
            viewer_status("[*] Starting Modem USB");

            while(wv_dial->isOpen()){
                wv_dial->kill();
                wv_dial->close();
                wv_dial->terminate();
            }
            counter_modem = 0;
            flag_tty= true;
        }

        if(flag_tty){
            flag_tty = false;
            qDebug("aqui llego modem se activo");
            wv_dial->setProgram("python3");
            wv_dial->setArguments(QStringList() <<"/jpt/jptmonitoring/External/connectModem.py");
            wv_dial->start();
            viewer_status("[o] Started Modem USB");
        }
    }
    counter_modem++;
}
