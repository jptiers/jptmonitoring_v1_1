#ifndef JPTALERTDIALOG_H
#define JPTALERTDIALOG_H

#include <QDialog>

namespace Ui {
class jptalertdialog;
}

class jptalertdialog : public QDialog
{
    Q_OBJECT

public:
    explicit jptalertdialog(QWidget *parent = nullptr);
    ~jptalertdialog();

signals:
    void close_message();
    void answer(bool res);

public slots:
    void message_from_moni(QString label, bool type);
    void action_cancel();
    void action_yes();

private:
    Ui::jptalertdialog *ui;
};

#endif // JPTALERTDIALOG_H
