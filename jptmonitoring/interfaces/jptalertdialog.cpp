#include "jptalertdialog.h"
#include "ui_jptalertdialog.h"

jptalertdialog::jptalertdialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::jptalertdialog)
{
    ui->setupUi(this);
    setFixedSize(378, 111);
    setWindowTitle("Message");
    connect(ui->btn_cancel, SIGNAL(clicked()), this, SLOT(action_cancel()));
    connect(ui->btn_yes, SIGNAL(clicked()), this, SLOT(action_yes()));

}

jptalertdialog::~jptalertdialog()
{
    delete ui;
}

void jptalertdialog::message_from_moni(QString label, bool type){
    if(label == "Database cleaned sucefully!"){
        ui->btn_cancel->setText("Close");
    }else{
        ui->btn_cancel->setText("Cancel");
    }
    if(type){
        ui->label_message->setText(label);
        ui->btn_yes->setEnabled(true);
    }else{
        ui->label_message->setText(label);
        ui->btn_yes->setEnabled(false);
    }
}

void jptalertdialog::action_cancel(){
    emit answer(false);
}

void jptalertdialog::action_yes(){
    emit answer(true);
}
