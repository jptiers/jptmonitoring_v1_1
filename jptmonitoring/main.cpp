#include "jptmonitoring.h"
#include "_sentence_.h"

#ifdef _COMPILE_GUI
    #include <QApplication>
#else
    #include <QCoreApplication>
#endif

int main(int argc, char *argv[]){
#ifdef _COMPILE_GUI
    QApplication a(argc, argv);
    jptmonitoring w;
    w.show();
#else
    QCoreApplication a(argc, argv);
    jptmonitoring w;
#endif
    return a.exec();
}
