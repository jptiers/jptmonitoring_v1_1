**Version 2.1**

-Version en que se recibe el software en Noviembre del 2019.

**Version 2.2**

-Mejora de comunicacion con el widget SWC, cambiando la forma en la que recibe la trama y de como se procesa (Nueva trama de corte).

-Primera insercion del protocolo de transmision de modbus, directamente desde QT.

**Version 2.3**

-Implementacion de contadores de datos, y almacenados en archivos planos:
    *Datos ingresados correctamente en la BD.
    *Datos que no fueron ingresados en la BD.
    *Datos enviados correctamente a la nube.
    *Datos que no fueron enviados correctamente a la nube.

-Envio de trama con los dato de los contadores.

-Mejora de comunicacion con la nube, mitigando la desconexion continua.

**Version 2.4**

-Adaptacion de modbus por medio de script de pyhton.

-Actualizacion de la base de datos para la tabla modbus y datos de configuracion.

-Escoger el tipo de transmision entre RS232 o RS485.

-Protocolo de transmision de modbus con RTC y TCP/IP.

-Limpieza de base de datos desde la interfaz.

-Adicion de recepcion de trama de SWQ.

-Configuracion para conexion de widget SWQ.

-Recepcion de nuevas tramas de equipos (Spider, Tanks, Levels, Pluviometers).

-Filtro para transmitir datos a los widgets que realmente corresponden.


